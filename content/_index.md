---
title: Welcome to my corner of the web.
slug: mattstine-com
date: "2023-03-13"
---

Hi, I'm Matt Stine (he/they) &#x1F468;&#x200D;&#x1F4BB;, and I'm currently working as a Senior Principal Software Architect at a rather large bank &#x1F3E6;. You can find out more by reading my [CV](/cv.html).

These days I reside in Olive Branch, Mississippi, with my wife Jillian Cantor &#x1F469;&#x200D;&#x1F3EB; and our dogs Jasper and Gerald &#x1F436;&#x1F436;. If you don't recognize Olive Branch, I'm just a stone's throw (approximately two miles/3.23 kilometers) from Memphis, Tennessee. It's certainly interesting to be able to take a short hike up the road, straddle the state line, and not know which foot has fewer basic human rights!

So why am I here? Well, my four wonderful kiddos are still in this neck of the woods for a few more years at least. As soon as the last of them enrolls in university, I'm pretty sure we're getting out of Dodge. 

This site used to be all about "repping my personal brand." I've since realized that I don't want to dehumanize myself by reducing myself to a brand. So many of the things we do without thinking are ingrained in us by an all-pervasive capitalist society (society - not economic system - there's a difference), and over the past few months I've started to challenge those thought and behavior patterns.

Going forward, I want this site to be more like what I remember my websites being in the late 1990s. They were conversations with whomever happened upon my doorstep. They weren't about tracking visits (beyond those silly mileage counters) or capturing leads. Most writing attached to my "day profession" of software engineering will land here. If you're looking for my spirtual content, you can find that on Substack at [Contemplating Resonance](https://contemplatingresonance.substack.com/).

I hope you enjoy your visit and find something that strikes your fancy. If you'd like to chat about anything you see, you'll find all of my contact details in the site footer. Cheers!
