---
title: "How Software Engineers Succeed by Selecting Tech That Sucks the Least"
date: 2021-08-20T00:00:00-05:00
categories: ["atomic-essay","software-engineering","principles","innovation-tokens","boring-technology","technology-tire-fires"]
---

*__Your technology organization cannot afford unlimited innovation.__*  
    
Dan McKinley coined the term **innovation token** to model this concept. Every organization gets a fixed supply of approximately three innovation tokens, and it can spend them on anything. As your organization grows in maturity, you might earn a few more tokens. But for the foreseeable future, the supply is fixed.  
    
How does this work in practice? Well, let's start spending tokens:  

- Let's choose **Elixr** for our backend services.  
- For our mobile apps, I think we'll go with **Flutter**.  
- And for our databases, I'm really excited about **CockroachDB**.  
    
That's it! We're out of innovation tokens. But wait! I'm pretty sure we can squeeze in **Svelte** to build our web app!  
    
> That vapor currently hitting your nostrils is the smoke rising from a technology tire fire.  

### *Overspending innovation tokens leads to technology tire fires.*  
    
Why is this the case?  
    
New and shiny technologies can be great. They might even be perfect for the problem you're trying to solve. But they also come with an invisible laundry list of unknown unknowns. They just haven't been deployed as much as the tried and true.  
    
**Dan's recommendation? Choose boring technologies.**

Technologies with well-understood operational characteristics and capabilities, and very few unknown unknowns:  

- Java  
- Spring  
- Angular  
- PostgreSQL  
    
*__These aren't sexy. But they've all gotten their respective jobs done for a long time in a lot of systems built by a lot of teams.__*  
    
Ultimately, your job is to use technology to keep your company winning in the marketplace. And that means that **the right tool for the job is probably the one that sucks the least**.  
### **So always ask, and answer carefully, the following questions before adding to your tech stack:**  

- How would you solve your problem without changing your tech stack?  
- What about your current tech stack makes solving the problem impossible?  
    
*__You just might be surprised by your answers.__*
