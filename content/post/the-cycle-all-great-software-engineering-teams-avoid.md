---
title: "The Cycle All Great Software Engineering Teams Avoid"
date: 2021-08-30T00:00:00-05:00
categories: ["atomic-essay","software-engineering","hyper-enthusiasm","embracing-constraints","no-silver-bullet"]
---

***Our industry is possessed by a deadly cycle.***
  
As we build software systems, we always run into complexity and problems. We see happier teams using a shiny new technology, and we run away to its greener pastures. Eventually, we discover the same complexity and problems that made us run away in the first place.  
  
**I've seen this cycle multiple times.** 

- From Java, EJBs, and Spring to Ruby and Rails
- From Ruby and Rails to Node.js
- From Node.js to Go
  
**In each case, a transition from basic web applications to enterprise software systems catalyzed an exodus.** They added additional hard requirements that basic web applications often didn't need. As we built capabilities into our frameworks to deal with this additional complexity, they became more difficult to use.  

## This triggered what Bruce Eckel called  *the departure of the hyper-enthusiasts* .
  
The hyper-enthusiasts then seed the formation of a new technology tribe, and they start recruiting new members. **You'll recognize it in the ubiquitous *“Why I left X for Y”* blog posts.** For them, technologies are not only tools, they are also where they find their identities.  
  
Unfortunately, the majority of the original tribe remains. It is still trying to solve problems, but without the help of the talented but impatient hyper-enthusiasts.  

## How do we break this cycle?
  
If you see in yourself the pattern of the hyper-enthusiast, here are some suggestions:  

### #1: Learn to Love Constraints
  
Constraints are powerful. **They are what allow a technology to make you promises it can keep.** Infinite flexibility comes at a high cost, often paid with long development cycles, unreliable systems, and errors that are tricky to diagnose.  
### #2: Learn to Love Doing Hard Things
  
If software engineering in complex domains at scale was easy, everyone would be doing it. **Doing hard things and learning the associated lessons are what move our tribes forward.**  

### #3: Learn that Greener Pastures are an Illusion
  
There’s a huge difference between “Hello World” and the real world. **What you see from the outside looking in is the easiest thing you'll ever see.** There always will be dragons lurking behind the scenes  

### #4: Learn to Listen to Fred Brooks
  
In 1987, he taught us there’s *No Silver Bullet*. I don’t care how many new technologies you adopt. **You’re never going to find a technology that solves all of your problems but doesn’t create any new ones.**  
  
***So stop running away, and break the cycle.***
