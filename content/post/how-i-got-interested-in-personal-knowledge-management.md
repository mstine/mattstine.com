---
title: "How I Got Interested in Personal Knowledge Management"
date: 2022-08-18T00:00:00-05:00
categories: ["atomic-essay","pkm","gtd","basb"]
---

I wouldn't call myself an expert in Personal Knowledge Management (PKM).  
  
However, I have honestly spent countless hours reading and learning about PKM. And I have been practicing various forms of PKM in my life for the better of 15 years.  
    
So many things have changed during those 15 years:  

- The things we choose to label as PKM  
- The techniques we use to practice PKM  
- The technologies we use to implement PKM  
    
But one thing hasn't changed, and I learned it early in my journey.  

## Here's how I first got interested in Personal Knowledge Management:  
    
I had reached the stage of my software engineering career where I played a tech lead role on a few apps. I also had a couple of org-level cross-cutting projects like improving our automated testing capabilities.  
    
I was having a hard time managing all of the things!  
    
While searching for help, I found various incarnations of David Allen's *Getting Things Done (GTD)* methodology. I bought the book, devoured it, and started trying to implement it precisely as David specified.  
    
It was awkward, and I spent more time managing the system than getting things done. But rather than giving up, I listened to the wisdom of others like Leo Babauta and adapted the method to my needs.  

## So my goal over the next year is:  
    
To make the elements of Building a Second Brain my own.  
    
Guess what? I don't exactly jive with Tiago Forte's style, and I hate Evernote as much as he loves it. But the principles and patterns he's added to the PKM ecosystem through Building a Second Brain (BASB) are just as malleable as GTD.  
    
And so, with a tool called Logseq, I'm slowly making BASB my own. Stay tuned for more!
