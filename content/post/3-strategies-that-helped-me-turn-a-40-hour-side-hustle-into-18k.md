---
title: "3 Strategies That Helped Me Turn a 40-Hour Side Hustle Into $18K"
date: 2021-08-21T00:00:00-05:00
categories: ["atomic-essay"]
---

***In 2015, I made $18,000 from a 56-page O'Reilly book titled Migrating to Cloud-Native Application Architectures.***
  
Time clocked from the first word typed to publication? **40 Hours.**  
  
I'm not here to sell you a magic formula. A surface-level analysis might suggest this was nothing but dumb luck. But it would be wrong.  
  
> Honestly, I never sought out the opportunity to write the book. **I did, however, engage in three strategic "luck creating" behaviors that I still continue.**  
## Anyone can be strategic!
  
These behaviors don't produce results. But they do produce opportunities!  

### 1. Practice Creating in Public
  
I loved attending conferences from the start. By 2007, I wanted to start speaking. I did two things:  

- I joined a **Toastmasters Club** so that I could create a fast feedback loop.
- I started a **Java User Group** so I could practice and meet other speakers.
  
This practice produced **a 2010 invitation to join the No Fluff Just Stuff (NFJS) tour**, which produced countless opportunities to practice speaking and writing.  

### 2. Pay Attention to Timing and Trends
  
In 2014, the industry was trying to figure out the cloud. I was working on Cloud Foundry at Pivotal, and I quickly found myself at the center of this conversation.  
  
The big question? **What does an app targeting the cloud look like?**  
  
After doing some research, I believed the ideal cloud app intersected directly with **a new architectural style people were calling microservices**. I told my boss we needed to be the microservices platform, and I wrote the first book with "cloud-native" in its title.  

### **3. Cultivate a Network of Like-Minded Professionals**
  
I haven't told you how any of this turned into my opportunity to write the book.  
*Every significant opportunity that has come my way has been delivered by someone I cultivated a relationship with years ago.*  

- Instead of hiding in my room after a long day of NFJS sessions, I joined the other speakers for dinner. That's where I met Neal Ford, **who _years later_ recommended me to O'Reilly Media, the book's publisher**.

- Instead of simply consuming Spring, I built relationships with its creators. **_Years later_ they offered me a job that eventually transformed into becoming the microservices expert at Pivotal**.
  
I started cultivating both of these relationships years before they produced opportunities. A strong professional network is like a well-tended garden. Cultivate it well, and it will eventually produce a bounty.

