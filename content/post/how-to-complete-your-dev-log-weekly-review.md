---
title: "How to Complete Your Dev Log Weekly Review"
date: 2022-01-28T00:00:00-06:00
categories: ["atomic-essay", "pkm", "dev-log", "software-engineering"]
---

Today I'm going to teach you how to begin your journey to harvesting insights from your dev log.
  
You should expect a healthy ROI from the time you spend capturing your daily activities. Simply writing down everything you do each day isn't enough. Reviewing and summarizing the week is like conducting a mini-retrospective with yourself. 


Unfortunately, most people never conduct a single weekly review.  

## Because they don't know how.
  
Not only that, but:  

- You lost track of time and you have to head home.
- You were coding down to the wire to prepare for that Friday deployment we won't talk about.
- Friday is a holiday, and you forgot to do it on Thursday.
- And failing those, no one wants a standing appointment on Friday afternoon!
  
Not knowing how coupled with these other bouts of resistance can make the prospect of beginning a weekly review practice seem daunting.   
  
Fear not. Just begin. And follow these three steps:  

### Step 1: Read each day's log.
  
You need to experience the day as it happened.  
  
Resist the temptation to simply skim your entries. Actually read them. Try to picture yourself doing each task. Did you miss any details? Do you have any new insight into any of your surprises?  

### Step 2: Write a summary of your key activities for the week.
  
Create a page for this week in your log, then walk back through each day, summarizing the key events.  
  
What problems did you solve? What incidents did you work? What features did you ship? What bugs did you fix?  

### Step 3: Move any leftover TODOs to your Weekly Review Note.
  
Move (don't copy) all of your open loops out of your daily pages so you can close the book on your week.  
  
Try as we may to complete them, we often end the week with unfinished tasks. You don't want to be digging through weeks of daily logs to make sure nothing was missed. By moving them to the week's note, you effectively archive those daily pages for good.
