---
title: "The Simple Mental Model Any Software Product Team Can Use to Unlock Success"
date: 2021-08-24T00:00:00-05:00
categories: ["atomic-essay","software-engineering","mental-models","problem-solving","inversion"]
---

Builders run into problems. If there's anything a product team should be good at, it's solving them!
  
But most of us rarely spend time sharpening our problem-solving saw.  
  
**Enter the concept of mental models.** Models give us abstractions of how one or more things actually work. They help us understand the world. George MacGill provides this analogy:  
  
> If your consciousness is the operating system, mental models are like apps, which you can use to help make decisions in whatever situations you find yourself in.  

## Today we're going to consider *Inversion*.
  
Charlie Munger developed this model to help him identify and remove obstacles to success. He turned problems upside-down, approaching them from the opposite end of where most people would start. According to Forbes, Munger is worth $1.9 billion, so he's likely solved a problem or two.  

## We work the problem backward rather than forward. Once we've identified the obstacles, simply avoiding them is our pathway to success.
  
Here are three examples of how to attack software product problems with Inversion:  

### We need to improve time to market!
  
*Inverted: What things will definitely slow down time to market?*  

- Slow feedback loops
- Manual testing and deployment processes
- Antiquated developer hardware

### We need to improve quality!
  
*Inverted: What things will definitely result in poor quality?*  

- High-friction testing environments
- "Clever" source code
- Poorly designed quality metrics

### We need to improve reliability!
  
*Inverted: What things will definitely result in unreliable systems?*  

- Poor error handling
- Single points of failure
- High-friction failover mechanisms
  
**In each of these cases, simply avoiding the obstacles we've identified will get us closer to our goals. Charlie Munger wrote: "It is remarkable how much long-term advantage people like us have gotten by trying to be consistently not stupid, instead of trying to be very intelligent."**  
  
  *__Starting tomorrow, see how far removing the stupid from your environment will get you!__*
