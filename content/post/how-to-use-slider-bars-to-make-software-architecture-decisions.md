---
title: "How to Use Slider Bars to Make Software Architecture Decisions"
date: 2021-08-18T00:00:00-05:00
categories: ["atomic-essay","software-engineering","decisions","software-architecture"]
---

Today, I'll teach you how to leverage The Slider Bar Principle to make better software architecture decisions.  
  
**The Slider Bar Principle teaches us that most software architecture decisions are fuzzy rather than binary. That is, the possible choices lie along a spectrum.**  
  
Understanding this principle is critical, but it's only the first step. Using it to guide your decision-making process is the only way it can positively impact your team's outcomes.  
  
Unfortunately, most teams get caught in the trap of binary decision-making.  

## And they go looking for villains and heroes.  
  
Recognize any of these false dichotomies?  

- Monoliths vs. Microservices  
- SQL vs. NoSQL  
- Public Cloud vs. Private Cloud  
    
Write a tweet painting one as the villain and the other as the hero and watch the engagement roll in. But we're here to write software, not build our brand.  
  
Here's how to start "slider barring" your decisions tomorrow:  

### **Step 1: Identify the Architectural Characteristics You Care About Most**  
  
Things like modularity, resiliency, scalability, security, etc. While there are dozens of characteristics, our success is usually driven by a few. Pick the ones that matter most.  

### **Step 2: Pick the Characteristic You Want To Vary**  
  
To have a slider bar, we need a characteristic we can slide. For this discussion, let's choose modularity.  

### **Step 3: Identify the Extremes and Waypoints**  
  
Identifying the extremes is the easiest part. For modularity, the obvious choices are monoliths and microservices.  
  
Waypoints are "steps along the way." Start with a monolith, and identify a choice that is one step closer to microservices.  

### **Step 4: Profile Each Waypoint's Impact**  
  
Examine the other characteristics you identified in Step 1. How are they impacted each time you slide the bar one waypoint?  

### **Step 5: Choose the Optimal Waypoint for Your Context**  
  
In most situations, one waypoint's profile will jump out as the best choice. Now, not only have you made the best possible decision, but you've also assembled the elements necessary to communicate that decision effectively.
