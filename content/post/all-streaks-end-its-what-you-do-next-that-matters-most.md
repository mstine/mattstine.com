---
title: "All Streaks End - It's What You Do Next That Matters Most"
date: 2021-09-08T00:00:00-05:00
categories: ["atomic-essay","baseball","streaks","consistency","trajectory"]
---

On September 20, 1998, Cal Ripken Jr. ended his record streak of 2,632 games played. ***In 1999, he had the highest batting average of his 21-year career.***
  
On July 17, 1941, Joe DiMaggio ended his record streak of 56 consecutive games with a hit. **He went on to win the 1941 World Series and American League MVP award.**  
  
On October 4, 1988, Orel Hershiser gave up his first run in 67 consecutive innings (regular season and playoffs). **He went on to win the 1988 World Series and the Cy Young, NLCS MVP, and World Series MVP awards.**  
  
***On September 4, 2021, I ended my Ship 30 for 30 writing streak at 20.***

## What am I going to do next?

- **I'm not going to beat myself up.** I just finished a stress-packed house moving project. Almost nothing about the process was smooth, and by the time I sat down on the night of September 4, my brain was toast. And it hasn't really started functioning again until this morning.
- **I'm going to keep writing.** I already have a great idea for tomorrow's atomic essay. It might be my best one yet.
- **I'm going to stay focused on my trajectory.** In the last 25 days (including today), I've published 21 times. If I repeat that performance, it will be 42 days of 50. Then 63 days of 75.
- **I'm going to find ways to improve.** I now know what a project like moving can do to my writer's brain. How can I manage future projects better? Should I queue up extra essays on the easier days? Should I alter my writing schedule (usually late afternoon) to compensate for project tasks?
  
***Above all, I'm going to stay focused on my identity. I am a writer. Writers write. They don't make excuses.***
