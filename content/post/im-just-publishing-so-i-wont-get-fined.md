---
title: "I'm Just Publishing So I Won't Get Fined"
date: 2021-09-02T00:00:00-05:00
categories: ["atomic-essay"]
---

> *NOTE: I thought about not re-publishing this one. It's a throwaway essay intended to keep a daily writing streak alive. But it's kinda funny...*

***In the spirit of Marshawn Lynch, I'm showing up because the Captain said I had to do this 30 days in a row.***

<iframe src="https://giphy.com/embed/kiBh0ERhYj1KPUZgKQ" width="480" height="470" frameBorder="0" class="giphy-embed" allowFullScreen></iframe> 

Have I mentioned I'm moving right now? Moving sucks. Especially when you can't put the rest of your life on hold and focus on just that.  

## Because it is all-consuming.
  
You find out at the 11th hour that the TV protector you bought on Amazon is worthless, because the movers won't accept liability unless you use **THEIR MORE EXPENSIVE COVER.**  
  
You find out that when you move from the west side of the county to the east side of the county, **literally every utility provider is different.**  
  
**And on that topic:** am I the only person that doesn't physically go to the power company to pay their bill? Because there were a lot of people there when I had to physically walk in to show my warranty deed. Ya know, so I can have electricity.  
  
You find out that half of your stuff is **too big for the small boxes and too heavy for the large boxes.**  
  
You find out that trying to write software with moving on the brain is **generally like being stuck in quicksand.**  
  
But somehow I've managed to not break this publishing streak. Today was the day I circled as the riskiest day of this cohort. And today's a big day: #20. **And I'm not about to break the streak now.**  
  
***So here I am. Publishing. Because it's what I do.***

