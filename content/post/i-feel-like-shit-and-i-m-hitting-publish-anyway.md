---
title: "I Feel Like Shit and I'm Hitting Publish Anyway..."
date: 2022-08-23T00:00:00-05:00
categories: ["atomic-essay","resistance"]
---

All I want to do right now is go to bed.  
  
I've worked my ass off on some intense software architecture initiatives over the last two days. My teenagers had their first football games of the season on Friday night and Saturday morning, and at least one of them now has the flu. My sleep is off. And now I think I might be getting sick also.  
  
I committed to publishing Monday through Friday this cohort, and I've already missed two days in a row.  

## Resistance believes it has already won another battle.  
  
I can find a million and one reasons not to be doing this.  
  
I have a great job doing work that I find interesting. I've built a strong enough personal brand as a technologist that I can usually score more traditional side-projects when I want them. I have a hectic family life. I love to read - just for me, not because I want to learn something to write about!  
  
Why the hell am I still doing this Ship 30 thing?  

## Resistance has all of the ammunition that it needs to keep me down.  
  
But here's the thing, and there's no way around this: I love sharing ideas with people and hearing what they think about what I have to say.  
  
And there's no better way (that I know of) to do that than to be a digital writer.  
  
Digital writing allows me to put my thoughts in front of what my metrics tell me are tens of thousands of people when I say something that resonates. There's no substitute.  

## And so, even though Resistance has me in a choke hold right now, I'm pushing through.  
  
Because if I skip today, I've got a feeling I'm just going to let that son of a bitch win. And I'm not going out like that. Peace.
