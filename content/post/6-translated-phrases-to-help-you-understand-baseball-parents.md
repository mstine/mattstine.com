---
title: "6 Translated Phrases to Help You Understand Baseball Parents"
date: 2021-09-11T00:00:00-05:00
categories: ["atomic-essay","baseball"]
---

***There is a foreign language spoken by the parents at youth baseball tournaments.***
  
I started drafting this piece as I was sitting at yet another opening tournament of what we call "Fall Ball." My son's starting at Third Base this year, and he's also getting some reps as a relief pitcher. As I waited for the game to start, I also waited for the "baseball parent-isms" to start flying.  

### I didn't have to wait long.
  
If you're not among the initiated to baseball-parent speak, ***allow me to provide you translations of the 6 most commonly heard phrases:***

## Good Eye / Way to Watch
  
Both are commonly shouted at a batter that has declined to swing at a ball. What I find strange is the fact that the pitch could have been thrown straight into the dugout and at least one parent would still exclaim "Good eye!"  

## Gotta Protect / Anything Close
  
You'll hear either of these once a batter gets two strikes on them. Protect is short for "protect the plate," which basically means, "Don't go down looking." If it's anywhere close to the strike zone, you better swing.  

## Heads Up!
  
It doesn't really matter where you are in the complex, if you hear this one, you may have a baseball coming directly at your head. We pack so many fields in close quarters that it could literally be coming from any direction.  

## Now You're Ready
  
This one still makes me scratch my head. I typically hear this one after a batter has swung at a bad pitch. I think it means, "OK, you've got the stupid out of your system. Now you can hit the next one." Please do correct me if I'm wrong.  

## Way to Battle
  
This one is my oldest daughter's personal favorite (this is sarcasm, also her personal favorite). During a particularly long at-bat, with a ton of fouled-off pitches, you'll eventually hear it. I guess if you imagine the bat to be a lightsaber, and the ball to be a training remote, it makes a bit more sense.  

## Make Him Pitch to You
  
Let's end on another head-scratcher. Make him pitch to you. Does he have a choice? This one feels like the inverse of "Anything Close." Meaning, don't make it easy on the pitcher by swinging at bad pitches. ***But even then, does he have a choice?***
