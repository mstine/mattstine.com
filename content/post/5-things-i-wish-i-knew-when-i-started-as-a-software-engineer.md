---
title: "5 Things I Wish I Knew When I Started as a Software Engineer"
date: 2021-09-01T00:00:00-05:00
categories: ["atomic-essay","software-engineering","no-silver-bullet"]
---

***When I got started, I didn't know a damn thing about software engineering.***
  
It was 2001. I'd just earned my Computer Science degree and was starting my first job. I'd taken the one software engineering course my university offered. I thought I was ready for any coding task they could throw at me.  
  
**I couldn't have been more wrong.**  
  
If you do the math, you can tell I'm 20 years into my career.  
  
**I have learned so much.** Many awesome things and many things I wish I could forget. All of them, the good and the bad, have made me the engineer I am today.
  
***If I could sit down and have a conversation with myself of 20 years ago, here's what I'd tell him.***  

## #1: There's No Silver Bullet
  
Yeah, I know you read that Fred Brooks essay in CSci 387. But you didn't internalize it. You're going to learn languages, patterns, frameworks, and processes. You'll become a Test-Driven Development zealot. And you're going to think, "If everyone would just do, ***we could crush it!***" 
  
Nope. I've seen it all, kid. Nothing can make a bad team produce good software fast.

## #2: Environments With Direct Access to the Business Are Rare
  
Yeah, I know your users work right down the hall. I know they've said drop by anytime. And you're gonna love it! But don't get used to it. This is not the norm in our industry. You got lucky and started at a unicorn.  
  
***In the future, if you're even allowed to talk to the business, getting on their calendar is going to be impossible unless you're late or something's broken.***

## #3: Being an Engineer That Can Talk to Humans Is a Force Multiplier
  
You're gonna go to some tech conferences. You're gonna see people give both great and shitty presentations. And you're gonna tell yourself, "I can do that." You're right. And it's going to be the difference in your career.  
  
***Putting yourself out there as a writer or a speaker in this industry is so rare, it instantly elevates your profile. Every opportunity you get will be linked to it in some way.***

## #4: Everything You Struggle To Promote Will Be Mainstream in 10–15 Years
  
Come down out of the clouds, kid. You're gonna promote a bunch of stuff, both at work and out on the road. People are going to heckle the shit out of you. They're going to tell you your ideas are dumb. A couple of them are going to hint at firing your ass.  
  
***And then they'll adopt your ideas. And they'll take credit for them. So make yourself comfortable knowing you made a difference, even when you don't get the recognition.***

## #5: You’re Going To Watch the Industry Reinvent the Wheel Multiple Times
  
The software industry truly is a Sisyphean nightmare. It takes a simple and powerful idea, and it bikesheds it up the hill, creating a snowball of complexity. Eventually, that snowball becomes way too heavy, and that hill becomes way too steep, and it rolls down and shatters at the bottom.  
  
***And then, the software industry picks up another simple, powerful idea and repeats the cycle.***
