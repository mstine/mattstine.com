---
title: "4 Things You Should Put in Your Daily Dev Log"
date: 2022-01-27T00:00:00-06:00
draft: false
categories: ["atomic-essay", "pkm", "dev-log", "software-engineering"]
---

To harvest meaningful patterns from your dev log, you'll need to capture the same things daily.
  
I've been engineering software for 21 years, and I'll share what I've routinely kept in mine. Your context is different, so your list will be also. Just make sure you capture the same things daily.  
  
Use this list as a skeleton to start from. It's easier when you aren't starting from a blank page!  

## Pull Requests
  
I keep track of every PR I submit, approve, or merge.  
  
Almost any work product I deliver or review flows through Git. My lists of PRs serve as a personal task-level kanban board of work in progress that I am connected to. These help me easily reconstruct a story of what I've done throughout the week.  

## Personal Interactions
  
I make note of every personal interaction I have, scheduled or otherwise.  
  
In addition to these notes, I keep a page for each person, and I link to that page from my daily notes. As I note patterns of interaction with a person, I'll describe those patterns on their page. Over time I build a library of every person I've collaborated with and what we worked on together.  

## Project Actions
  
I keep a running narrative of the things I'm working on.  
  
I keep a section for each thing I'm working on. These might be at the product or story level - it just depends. Each time I hit a stopping point or interruption, I make a note of what I've been doing and what I plan to do next. Guess what I use to inform my daily standup updates?  

## Perplexing Puzzles
  
I make a note of everything that surprises me.  
  
These could be strange program behaviors, blockers, production issues - anything unexpected. As I review these periodically, I ask myself how I can use what I've learned to improve my mental models.  
  
Hopefully this list helps you get past the starting line. Do this faithfully for several months and see where it takes you!

