---
title: "Egoless Programming #3: No Matter How Much Karate You Know, Someone Else Will Always Know More"
date: 2022-01-17T00:00:00-06:00
slug: egoless-programming-3-no-matter-how-much-karate-you-know
categories: ["atomic-essay","egoless-programming","humility-driven-development","software-engineering","principles"]
---

In 2001, Lamont Adams of TechRepublic chiseled what would become ten commandments of Gerald M. Weinberg's timeless wisdom: **Egoless Programming**. Today we're going to learn its third commandment:  
  
> *No matter how much "karate" you know, someone else will always know more.*  
  
First of all, this is a good thing. My friend Neal Ford has said that as soon as you are the smartest person in the room, it's time to look for another room. You want to be challenged, and you want to be humble and teachable enough to continue learning from those challenges.  
  
Here are a few opportunities to learn from the *senpai* in your midst:  

## Someone has always been around the company longer.  
  
Unless you came with the building, they're lurking somewhere.  
  
While longevity doesn't necessarily imply wisdom, it often implies a larger collection of stories. Want to know why things are the way they are? Ask these people. They usually know where the proverbial bodies are buried.  

## Someone has always invested more personal time.  
  
There's always at least one person who spends every waking hour hacking.  
  
I'm definitely not encouraging you to be this person. Balance and moderation are important. But you don't have to be them to benefit from what they've already figured out.  

## Someone has always visited the other side.  
  
Whether you work for a traditional enterprise or an early-stage startup, you'll always find at least one person who's done the opposite.  

In case you weren't aware, these are very different environments. And each can benefit from the practices and techniques used in their foil. But it's the person who's had the job that knows what the blog posts won't tell you.  

## Someone always has another tool in their belt.  
  
Everyone takes a different path to where they are, and they've needed to master different tools along the way.  
  
After 20+ years of software engineering, not a week goes by that someone doesn't recommend a tool I've never encountered. I keep them all in a folder called "Tools for a Rainy Day." Many have been promoted to my daily workflow.  

## You'll always be "that someone" to someone else.  
  
While you're looking around for your *senpai*, someone else is looking for you.  
  
Paying it forward is one of the best ways that you can keep this industry growing. I owe so much of my growth as an engineer, architect, manager, and speaker to the *senpai* in my life, and I want to provide the same to others.  

## I've found these people in every job I've held.  
  
Take my advice: they represent your richest opportunities to learn and improve. **Don't waste them.**
