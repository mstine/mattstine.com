---
title: "Want to Ship Code Faster? Stop Task Switching"
date: 2021-10-08T00:00:00-05:00
categories: ["atomic-essay","seven-wastes-series","waste","agile","lean"]
---

***Task Switching is the sixth of the Seven Wastes of Software Development.***
  
The Seven Wastes were identified by Mary and Tom Poppendieck, the foreparents of Lean Software Development. It grew out of their efforts to understand and apply the principles of Lean Manufacturing to software engineering. **Waste elimination is the essence of Lean's archetype, the Toyota Production System:**  
  
> *All we are doing is looking at the timeline from the moment a customer gives us an order to the point when we collect the cash. And we are reducing that timeline by removing the non-value-added wastes.*

## Eliminating waste is about increasing speed.
  
So how does Task Switching slow us down?  
  
**Task Switching is software development's analog to manufacturing's Motion.** Motion refers to the necessity for humans to move about the factory floor more than necessary to accomplish their work. The core value of the assembly line is the parts coming to the worker rather than the worker going to the parts. McDonald's nailed this concept in the 1940s with its mechanized kitchen called the Speedee Service System.  
  
***Both Task Switching and Motion introduce unnecessary interruptions to the flow of work.***

## So how does Task Switching slow software engineers down?
  
In 1987, Tom DeMarco and Timothy Lister highlighted the cost of Task Switching in their influential book, *Peopleware: Productive Projects and Teams*.  
  
**DeMarco and Lister described the phenomenon of flow** - the holy grail of coding. Flow is a state of extreme productivity during which time disappears, and your computer becomes an extension of your mind. Flow is when value is manufactured.  
  
Getting into flow is extremely difficult. It takes about 15 minutes to load our problem into our brain. During that 15 minutes, we don't get anything tangible done! ***Once there, any interruption restarts the cycle. 4 interruptions cost an hour of productivity. 32 interruptions cost a day.***

## So how can we beat Task Switching?

- If you must work on multiple projects, work on one at a time. Maximize your flow by minimizing your context switching.
- If your team is required to handle interruptions (perhaps for community engagement), consider starting a rotation. Make sure everyone can access the knowledge they need to handle the interruptions. Otherwise, this solution will quickly break down.
- Ensure everyone can access the knowledge they need to complete their work, preventing task switching to search for information.
- ***Eliminate unimportant work and interruptions! If it isn't delivering value, stop doing it!***

