---
title: "Egoless Programming #1: Understand and Accept That You Will Make Mistakes"
date: 2022-01-15T00:00:00-06:00
slug: egoless-programming-1-understand-and-accept-that-you-will-make-mistakes
categories: ["atomic-essay","egoless-programming","humility-driven-development","software-engineering","principles"]
---

In 2001, Lamont Adams of TechRepublic chiseled what would become ten commandments of Gerald M. Weinberg's timeless wisdom: **Egoless Programming**. Today we're going to learn it's first commandment:  
    
> *Understand and accept that you will make mistakes.*  
  
This one should be obvious. But we engineers often behave as if our fallible human nature does not extend to our chosen craft. And as much as I'd love for that to be true, it simply isn't.  
  
Care for some examples?  

## You will break the build.  
  
You'll make your machine different from the build machine, and you'll forget to check.  
  
One time I was building a client to an API that in the absence of proper credentials could still read from a local cache. My tests encoded assumptions based on the data in my workstation's cache. I didn't check in the cache. Guess what failed?  

## You will write bugs.  
  
Computers do exactly what you tell them to do, not what you thought you told them to do.  
  
One time I wrote a bug that caused a unit test to delete all of my source code. When I finally figured out what I did, I was astounded by how silly of a mistake I had made. But hey, I lived to tell about it (thank you, Git!).  

## Some of your bugs will make it to production.  
  
Production is unfortunately sometimes the place your bug encounters the conditions necessary for it to manifest.  
  
One time I was processing some data returned from an API. I called a method that I didn't realize would fail silently in the presence of a specific class of remote server errors. One of these errors appeared for the first time in production, and I overwrote our dataset with an empty file. Oops.  

## You will do all of these things your entire career.  
  
It's important to understand that these aren't junior or senior mistakes. They are human mistakes.  
  
***Every mistake on this list was made in the last year by a senior engineer: me.***

## Anyone who says they don't do any of these things anymore is lying.  
  
So make your mistakes, learn from them, and try not to make them again...often.
