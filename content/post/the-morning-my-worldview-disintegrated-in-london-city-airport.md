---
title: "The Morning My Worldview Disintegrated in London City Airport"
date: 2021-08-15T00:00:00-05:00
categories: ["atomic-essay","deconstruction","faith","politics","exvangelical"]
---

I remember sitting in London City Airport at 5:00 AM, watching TV through a coffee-fueled haze as the 2016 U.S. presidential election returns scrolled across the chyron.  

**The impossible was now something I had to accept.**  

A world that I thought I knew. That I thought I understood, was no more.  

A worldview that I had fortified for fifteen years, in an instant, had disintegrated.  

A worldview that had been instilled in me by my parents and the church in which I grew up.  

A worldview that I didn't embrace, until at age 22 I did what "grownups" in the southeastern U.S. do: built my life around the church.  

**My time as a conservative, evangelical Christian was over.**  

> I did not know then that the journey I was beginning had a name: **Deconstruction**.  

Deconstruction is a philosophical method born from the work of Jaques Derrida. More recently, it has been embraced by a global community trying to make sense of life before and after a worldview disintegrating moment. Thomas Kuhn described a similar phenomenon in *The Structure of Scientific Revolutions* as a paradigm shift: a sometimes violent transition from one scientific system to another, occurring when the previous system is no longer fit to model reality.  

**For me, the election of Donald Trump was the catalyst that caused my paradigm - my mental model of reality - to violently implode.**  

In the coming weeks, I'm going to write about many different facets of deconstruction, ultimately because I'm trying to transition to reconstruction. I want to build a new mental model of reality, using my writing to clarify my thinking. And I hope that you too will gain value from shadowing my journey.  
