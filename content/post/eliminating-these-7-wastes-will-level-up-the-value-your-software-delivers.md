---
title: "Eliminating These 7 Wastes Will Level Up the Value Your Software Delivers"
date: 2021-09-10T00:00:00-05:00
categories: ["atomic-essay","seven-wastes-series","waste","software-engineering","lean","agile"]
---

***Waste elimination is critical to delivering value through software.***
  
In the late 1940s, Toyota had a problem. Manufacturing was expensive, so prices were high. But the typical car buyer in Japan was light on cash. **Reducing the cost of manufacturing was the only way to sell cars.**  
  
Mass production could have solved the problem, but Japan's economy wasn't large enough to create demand for thousands of cars. Toyota had to find another way, and they turned to Taiichi Ohno.  
  
**Ohno's Toyota Production System (TPS) was that other way.** This is how he described it:  
  
> *All we are doing is looking at the timeline from the moment a customer gives us an order to the point when we collect the cash. And we are reducing that timeline by removing the non-value-added wastes.*
  
Another TPS forefather, Shigeo Shingo, identified **seven major categories of manufacturing waste:**  
- Inventory
- Overproduction
- Extra Processing
- Transportation
- Waiting
- Motion
- Defects
  
Mary and Tom Poppendieck, the foreparents of Lean Software Development, **translated these into the seven wastes of software development.**  
  
***Eliminating these seven wastes from your software engineering ecosystem will immediately level up the value you deliver to your organization:***

## #1: Partially Done Work
  
Partially done work often becomes obsolete long before it's finished, and it always gets in the way. You'll find it in uncoded requirements, uncommitted code, untested code, undocumented code, undeployed code, commented-out code, and copy/paste reuse.  

## #2: Extra Features
  
Extra features bloat products, increase their cost, and may never be used! Extra code must be tracked, compiled, integrated, tested, and maintained. It increases complexity and adds potential failure points.  

## #3: Relearning
  
Learning creates value. Relearning creates waste. Why? Because we've lost the value created by the original learning (but not the cost). The more times we repeat this cycle, the more waste we create.  

## #4: Handoffs
  
Handoffs happen when we transfer requirements, code, or other artifacts to an individual/group that wasn't part of its creation. Most knowledge is difficult to communicate, so knowledge is always left behind.  

## #5: Delays
  
Anything that delays the start or completion of a value-adding activity. Delays prevent your customer from realizing value as soon as possible, and they introduce discontinuity into your process.  

## #6: Task Switching
  
Effective software development requires deep, concentrated thinking. Interruptions are the ultimate enemy of deep, concentrated thinking, and any task switch constitutes an interruption.  

## #7: Defects
  
This waste should be self-evident, and defects are going to happen (nobody's perfect!). We can quantify the waste as the product of the defect's impact and the time it sits undetected.

