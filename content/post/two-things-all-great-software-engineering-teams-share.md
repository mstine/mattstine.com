---
title: "Two Things All Great Software Engineering Teams Share"
date: 2021-08-16T00:00:00-05:00
categories: ["atomic-essay", "software-engineering", "teams", "principles"]
---

## What is a great software engineering team? 
 
- It routinely delivers differentiated value to its customers.  
- It can go fast forever.  
- It can respond to changing market conditions and move in the right direction.  
- It can deliver software that runs on day one and keeps running on day two.  
    
Teams like this have many common characteristics, but let's focus on two:  
    
## They recognize the primacy of principles.  
    
Stephen Covey once said, "there are three constants in life...change, choice, and principles."  
    
In our world, change is constant. Most things we do to move our industry forward help us handle change better. In the face of competing tradeoffs, we're forced to make choices. And when those choices align with principles, they normally lead to successful outcomes.  
    
Principles govern every creative endeavor. You can't break them. You can only break yourself against them.  
    
Great software engineering teams align themselves with principles.  
    
## They avoid the temptation of greener pastures.  
    
As we build software systems, we inevitably encounter complexity and problems that transform coding from delight to drudgery.  
    
We're not having fun anymore. We look up from our daily work and notice a new technology has appeared. And teams using that technology appear to have simpler, more productive lives.  
    
And so we flee to "greener pastures." And it is nice for a while. But then things change. And we fail.  
    
And we discover the same complexity and problems that made us flee in the first place. Complexity and problems that are simply inherent to the business of building software. Those greener pastures were an illusion.  
    
Great software engineering teams persevere through complexity and problems.

