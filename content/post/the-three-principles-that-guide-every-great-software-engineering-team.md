---
title: "The Three Principles That Guide Every Great Software Engineering Team"
date: 2021-08-17T00:00:00-05:00
categories: ["atomic-essay","software-engineering","principles","teams"]
---

## Great software engineering teams recognize the primacy of principles.

A few decades ago, everyone wanted to visit Toyota. It had become the canonical example of a great manufacturing company. And Toyota invited everyone to visit, even its competitors.  

Why?  

Because Toyota knew that its competitors would do everything they could to duplicate its practices and tools, rather than understanding the principles of continuous learning and improvement.  

> Toyota knew that by the time its competitors successfully duplicated its practices and tools, it would have learned and improved.  

So what are the principles that guide great software engineering teams?  

## The Slider Bar Principle

Teams often seek binary answers to fuzzy questions, creating false dichotomies.  

*Fuzzy questions have fuzzy answers.*  

These answers lie along a spectrum, or a "slider bar." The answer isn't at the extremes. It's actually somewhere in the middle.  

Are you seeking the best answer for your context?  

## The No Free Lunch Principle

This is a common way of saying that everything has a cost.  

*All possible choices come with a set of tradeoffs.*  

You'll get this, but you'll give up that. It doesn't matter what practices or technologies you adopt. They always cost you something.  

Are you coming out ahead?  

## The Innovation Token Principle

No organization can afford unlimited innovation.  

Dan McKinley coined the term "innovation token" to model this concept. Every organization gets a fixed supply of three innovation tokens, and it can spend them on anything.  

*Overspending innovation tokens leads to technology tire fires.*  

His recommendation? Choose boring technologies. Technologies with well-understood operational characteristics and capabilities, and very few unknown unknowns.  

Are you choosing the new and shiny? Or the tried and true?  
