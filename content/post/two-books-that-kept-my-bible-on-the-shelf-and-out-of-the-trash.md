---
title: "Two Books That Kept My Bible on the Shelf and Out of the Trash"
date: 2021-08-22T00:00:00-05:00
categories: ["atomic-essay","deconstruction","faith","exvangelical"]
---

**As I began my deconstruction journey, I started asking a lot of questions.** I had so many that I organized them with a Trello board. As I studied the board, one of the questions leaped from the screen:

## Is the Bible inerrant and infallible?
 
Preachers and teachers had answered "YES!!!" my entire life. But for a brief summer encounter with wonderful people who practiced what the same preachers and teachers called "demonic religions," it was difficult to hear anything else. At least this was my experience growing up in the buckle of the Bible Belt.  
 
The more I reflected on my spiritual journey, the more I realized that my entire worldview was a house of cards built on the foundation of biblical inerrancy and infallibility.  

## Definitively answering this question would be the beginning and the end of my deconstruction journey.

Fortunately, about that time, an acquaintance recommended two books that for me became like maps to buried treasure:  

### *The Bible Tells Me So*  by Pete Enns

Pete dismantles the idea that the Bible is something that we need to defend, directly contradicting the endlessly repeating exhortation of my now estranged conservative Christian friends. As the late Rachel Held Evans so beautifully wrote, "Enns...teaches us to love it for what it is, not for what we try to make it."  

### *What is the Bible*  by Rob Bell

I was excited to read Rob Bell for the first time. Why? Because those same pastors and friends had continually warned me to stay away from Rob's "heretical teachings." What I learned from this book is that the Bible is not an instruction manual from God. Rather, it is a distinctly human book - a story of a people and their unfolding understanding of themselves and who they believed God to be.  

**Neither of these books directly confront my question. Not really. But they did help me decide to keep my Bible on the shelf and out of the trash.**  
  
Why?  

***Because they taught me the Bible isn't a plan for your life, a love letter, or any other tired cliche. Rather, it is a story like any other human story, one that invites us to ask it questions, reflect on its answers, and use what we learn to become better versions of ourselves.***
