---
title: "Want to Ship Code Faster? Minimize Partially Done Work"
date: 2021-09-20T00:00:00-05:00
categories: ["atomic-essay","seven-wastes-series","waste","agile","lean"]
---

***Partially Done Work is the first of the Seven Wastes of Software Development.***
  
The Seven Wastes were identified by Mary and Tom Poppendieck, the foreparents of Lean Software Development. It grew out of their efforts to understand and apply the principles of Lean Manufacturing to software engineering. **Waste elimination is the essence of Lean's archetype, the Toyota Production System:**  
  
> *All we are doing is looking at the timeline from the moment a customer gives us an order to the point when we collect the cash. And we are reducing that timeline by removing the non-value-added wastes.*  

## Eliminating waste is about increasing speed.
  
So what about partially done work slows us down?  

**Partially Done Work is software development's analog to manufacturing's Inventory.** Managing high amounts of inventory increases costs and hides process inefficiencies. By minimizing inventory, we can decrease costs and highlight (and eliminate!) process inefficiencies.  

## What's wrong with Partially Done Work?

- **It often becomes obsolete long before it's finished.** Until software encounters real users, you don't know if you've solved their business problem. And it's entirely possible that by the time you finish, the goalposts will have moved.
- **It always gets in the way of value-adding work.** Have you ever had to shelve code to get back to a stable state? Have you ever been afraid to push a code commit before quitting for the day? How long did you let that go? How much harder did it get the longer you waited?

## So what are some examples of partially done work?

- Uncoded Requirements
- Uncommitted Code
- Untested Code
- Undocumented Code
- Undeployed Code
- Commented Out Code
- Copy and Paste Reuse
  
***Spend the time necessary to finish each of these tasks before moving on, and you'll stop piling up inventory that will get in your way.***

