---
title: "Want to Ship Code Faster? Stop Building Extra Features"
date: 2021-09-21T00:00:00-05:00
categories: ["atomic-essay","seven-wastes-series","waste","agile","lean"]
---

***Extra Features are the second of the Seven Wastes of Software Development.***
  
The Seven Wastes were identified by Mary and Tom Poppendieck, the foreparents of Lean Software Development. It grew out of their efforts to understand and apply the principles of Lean Manufacturing to software engineering. **Waste elimination is the essence of Lean's archetype, the Toyota Production System:**  
  
> *All we are doing is looking at the timeline from the moment a customer gives us an order to the point when we collect the cash. And we are reducing that timeline by removing the non-value-added wastes.*

## Eliminating waste is about increasing speed.
  
So how do Extra Features slow us down?  
  
**Extra Features are software development's analog to manufacturing's Overproduction.** If you examine the other six wastes, you'll see they're primarily concerned with how we produce products. Extra Features and Overproduction are concerned mainly with what products we make. If we're wasteful in building necessary features, we'll be wasteful in building extra features.  
  
***Extra features drive an increase in all of the other wastes!***

## What's wrong with Extra Features?
  
**Easy. All extra code in our software:**  

- must be tracked
- must be compiled
- must be integrated
- must be tested (every time the code is touched!)
- must be maintained (for the life of the system!)
- increases complexity
- adds potential failure points
- and likely will become obsolete before it's used!
  
***If you're babysitting extra code, you're slowing down value delivery!***
  
As Gordon Bell memorably said:  
  
> *The cheapest, fastest, and most reliable components of a computer system are those that aren't there.*

## So what are some examples of Extra Features?

- **Pet features** that influential stakeholders insist on including.
- Features we could eliminate with faster feedback cycles.
- Features snuck in by developers padding their resumes with the latest new and shiny tech *(a.k.a. Resume-Driven Development)*.
- That feature the customer never asked for, but you "know" they're going to need.
  
***And finally, that feature that sounded good to everyone, but after six months, no one has used it!***
