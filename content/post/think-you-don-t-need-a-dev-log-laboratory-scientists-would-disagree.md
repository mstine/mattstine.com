---
title: "Think You Don't Need a Dev Log? Laboratory Scientists Would Disagree"
date: 2022-02-05T00:00:00-06:00
categories: ["atomic-essay","pkm","dev-log","software-engineering"]
---

Laboratory science has much in common with software engineering:  

- We take a new language or technology and experiment with it to see what it can do and how it performs.  
- When we practice Test-Driven Development, every failing test that we write encodes a hypothesis: that the test actually will fail!  
- We gather and study data obtained from our system and its surrounding environment as we troubleshoot incidents. We hypothesize causes, and we try to reproduce results.  

## So if software engineering has so much in common with laboratory science, why shouldn't we try to leverage some of its best practices?   
    
Here are five quotes from scientists illustrating the importance of laboratory notebooks, their analog to a daily dev log:  

### Matteo Falessi, postdoctoral researcher, ENEA Frascati Research Centre  
    
> *...a notebook is essential for keeping track of my attempts to solve a problem. Without well-written notes, I would probably do the same calculations many times.*  

### Caroline Lynn Kamerlin, professor, Uppsala University  
    
> *A good notebook makes it possible to quickly see what experiments were done and with what aims, reevaluate old data in the light of new findings, and pick up the work where you left it.*  

### Jeremy C. Borniger, postdoctoral fellow, Stanford University  
  
> *At some point in your career, you may also have to use it to provide evidence that an idea really was "yours" or to lay claim to your intellectual property.*  

### Valentina Ferro, Ph.D. and freelance science illustrator  
  
> *Over time, my lab notebook also evolved into a tool to keep track of my ideas. You might not want to change your design or method right in the middle of an experiment, but your notebook is the perfect place to note what you may want to try next.*  

### Caroline Bartman, postdoctoral fellow, Princeton University  
  
> *Lab notebooks are the most useful resource that you can create for yourself in the lab, and creating a good one requires a bit of effort every day. Once you get into the habit and have a system you like, it becomes less onerous.*  
    
**Quotes:** [https://www.science.org/content/article/how-keep-lab-notebook](https://www.science.org/content/article/how-keep-lab-notebook)

