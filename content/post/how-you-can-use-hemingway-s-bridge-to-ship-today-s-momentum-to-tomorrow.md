---
title: "How You Can Use Hemingway's Bridge to Ship Today's Momentum to Tomorrow"
date: 2022-08-24T00:00:00-05:00
categories: ["atomic-essay","pkm","strategies","creativity","writing"]
---

Today I'm going to tell you how to use a creative strategy called the Hemingway Bridge.  
  
I learned this strategy from reading Tiago Forte's book *[Building a Second Brain](https://bookshop.org/p/books/building-a-second-brain-a-proven-method-to-organize-your-digital-life-and-unlock-your-creative-potential-tiago-forte/18265370?ean=9781982167387)*, and I immediately started applying it to my daily work as a software engineer and architect. It has been life-changing! If you learn and use it, you'll start every day with a burst of momentum because you sent it to your future self as a gift.  
  
Unfortunately, most people keep working until they've exhausted their energy supply and have no momentum left.  

## Why? Because we believe we're supposed to give everything we have to the task at hand!  

- If the bug isn't fixed, and I've got time left in my day, I better keep troubleshooting.  
- If tickets are still in the queue, and I've got time left in my day, I better pull one more.  
- If the tests aren't passing, and I've got time left in my day, I better keep making code changes until they do.  
- If I didn't write that RFC, and I've got time left in my day, I better bang it out quick and send it to the mailing list.  
    
Do you feel exhausted just reading that list? I do. And I also know that not only will I not do an excellent job if I keep going, but I'll also arrive at my desk in the morning with an empty brain and no clue where to start.  
  
Here's how to stop doing that, step by step:  

### Step 1: Reserve some energy at the end of your day.
  
To send your future self momentum, you have to have the momentum to send.  
  
Hemingway would only end his writing sessions when he knew where he was going next with the plot. But he resisted the urge to keep going, instead choosing to invest that energy in preparing for the next session. This step is the hardest but also the most important.  

### Step 2: Write down where you are now.
  
We waste vital energy every morning gaining situational awareness.  
  
Let's take the bug fixing example: What bug are you fixing? What have you tried so far? What's your best guess as to what's happening? What questions do you still have? Who might you still need to ask for help?  

### Step 3: Write down where you want to go tomorrow.
  
We walk into the morning without a plan, so we usually wing it.  
  
Remember how we were reserving energy by not continuing with our work? Well, now you get to write down where you'd go if you kept going! Create an action plan. What will you do next, and how much progress do you want to make? Bonus points if you send your bridge to a colleague for input.
