---
title: "6 Critical Lessons I've Learned From 15 Consecutive Days of Publishing"
date: 2021-08-27T00:00:00-05:00
categories: ["atomic-essay","dissonance"]
---

> *NOTE (2023-04-19): I honestly don't recognize the person who wrote this essay, and this was written less than two years ago. I think I must have a hidden inner suceptibility to cultic religious groups/movements when I don't first recognize them as such. Because this is literally me preaching the Gospel of Ship 30 for 30. Ewwww.*

***About a month ago, I decided to give myself a firm kick in the ass to get back into writing.***
  
I needed some skin in the game, so serendipity sent me **Ship 30 for 30**. I was so impressed I made two big bets: **money and time**. For those keeping score:  
- $450
- Publishing an atomic essay on 30 consecutive days.
  
15 days in, that $450 is becoming one of my best cash investments. But more importantly, **those 15 consecutive days of publishing have taught me more about writing than any previous time investment.**  
  
Here are six critical lessons I've learned:  

## #1: Your Headline Writes Your Piece
  
Your headline communicates your ***WHAT, WHO, and WHAT'S IN IT FOR ME***. It's the only thing stopping your reader from scrolling onward. Iterate on your headline until it nails those answers, and only then start writing.  
## #2: Go to Your Audience
  
Social media has changed everything about communicating effectively online. ***Starting a blog and waiting for people to come is a dead strategy (Ironic, finding this on a blog. Yeah?).*** Twitter, LinkedIn, and Medium have become my go-to publishing platforms because the audience is already there.  
## #3: Engagement is Key
  
Publishing on social media is only half the battle. You also need engagement. But the algorithms operate on a *quid pro quo* basis: you get what you give. ***If you want your content to show up in feeds, you need to show up also.*** Reply to your readers. Engage with other creator's audiences.  
## #4: Keep it Simple Stupid
  
Your topics may be complex, but your writing shouldn't be. ***The best writers use basic vocabulary, write conversationally, and minimize the use of clever language.*** The purpose of your piece is to deliver value to your reader. It's not about you. It's about them.  
## #5: Write for Internet Readers
  
The days of continuous mountains of text are over. ***You're lucky if someone reads only your subheadings.*** Don't have subheadings? You're lucky if someone reads your piece at all. Format for quick scanning. Highlight important bits. Reveal information at a quick and steady pace.  
## #6: Show Up Every Day
  
This is the most important lesson. Make publishing a habit. Good habits performed daily pay compounding dividends. Writing is like working out. You won't see noticeable changes every day. There will be days you want to give up. ***But writing every day will put you on a trajectory. After months of investment, you won't recognize yourself or your audience.***  
  
***You'll just be that good.***
