---
title: "Six Developer Experience Killers Every Engineering Manager Should Avoid"
date: 2021-08-11T00:00:00-05:00
categories: ["ex-twitter-thread","software-engineering","developer-experience","reports-results-of-poll-or-informal-survey"]
---

> This was originally a Twitter thread. I've edited it a bit for style and format to make it a bit more friendly as a blog post.

I asked my Twitter following to share with me via DM their biggest frustrations with their day-to-day developer experience. I committed to report back with a summary of everyone's responses. Well, here's my report!

Some of their answers weren't a surprise. But not all of them!

# Here are 6 developer experience killers that every engineering manager should avoid:  

## *Can't you just...?*

I can't think of a worse way to invalidate a developer's expertise.  

You don't always have to like the answer a developer gives you, but this kind of response indicates a lack of trust in one's ability to generate, evaluate, and eliminate options.  

## *Here's your repurposed 5th gen Core i3 with 8 GB RAM. Enjoy!*

Going the cheap route with developer hardware is a quick and easy way to make everything else more expensive.  

If you don't want folks sitting around waiting, invest in quality hardware and software tools.  

## *Let's play two truths and a lie!*

Starting every meeting with an icebreaker is a great way to lose an otherwise productive team.  

While facilitation techniques can be great, especially with a new team building trust, using them inappropriately can destroy focus.  

## *So where does the fizzbuzz service live again?*

Would you rather developers ship code or quest for lost servers?  

I think I know the answer to that question. Foremost, champion discoverability in your environment. Failing that, take on these tasks so developers can focus.  

## *You estimated task X would take 2 days, but JIRA says it took...*  

Do you want predictable velocity? Nitpicking developer estimates won't get it.  

Software development almost always comes with uncertainty. We're creating something new, not assembling widgets on an assembly line.  

## *File those 50 tickets tomorrow so you can finish within the sprint!*

Tight coupling between systems forces developers to spend time coordinating rather than creating.  

Maximize the work your team can accomplish in isolation by enforcing clear boundaries and interfaces.  
