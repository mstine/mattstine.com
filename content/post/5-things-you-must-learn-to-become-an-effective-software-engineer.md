---
title: "5 Things You Must Learn to Become an Effective Software Engineer"
date: 2021-09-09T00:00:00-05:00
categories: ["atomic-essay","software-engineering","effectiveness","early-career"]
---

***As much as I loved my Computer Science curriculum at Ole Miss, it taught me nothing about being an effective software engineer.***
  
When I was in school, Computer Science programs were optimized for producing grad students. Grad students need to excel at research and publishing. Not shipping software.  
## My career experience has provided no evidence anything has changed.
  
***To be an effective software engineer, you must learn on the job.***
  
If you're lucky, you'll land at a company that emphasizes mentorship. I was not lucky, but I did like learning. I've spent the last 20 years learning both useful and useless software engineering skills.  
  
Rather than forcing you to figure out what's important, here are my top 5 must-have skills:  

## #1: Learn How to Learn
  
Are you a visual or auditory learner? Do you learn better starting with theory and moving to practice? Or are you the opposite? How are your note-taking skills? Do you understand active vs. passive reading? **Figure out the best times and methods for you to learn, and then always be learning.**  

## #2: Learn Your Tools
  
Would you hire a carpenter or surgeon that hasn't mastered the use of their tools? Why should it be different for a software engineer? The tools of our trade include IDEs, version control systems, command-line interfaces, and many more. **If you don't master your tools, you'll never master software engineering.**  

## #3: Learn the Stack Spectrum
  
Are you a front-end developer? Learn the basics of building backend services. Are you a back-end developer? Learn the basics of building single-page applications. Not everyone needs to become a full-stack engineer, but **everyone should have a basic working knowledge of the entire stack spectrum and how the pieces hang together.**  

## #4: Learn the Time Spectrum
  
Looking to the future is important, but don't neglect the past and the present. Our industry reinvents the wheel every 10-15 years. Your problems have likely already been solved a few different ways. Study prior art. Learn what worked and what didn't. Look at the mainstream. How does it incorporate or ignore the lessons of the past? **A solid understanding of the entire time spectrum of software engineering will prepare you to navigate future cycles.**  

## #5: Learn How to Troubleshoot
  
Your job is essentially solving business problems with technology. But if you can't solve technology problems, how do you hope to solve business problems? When things break, do you run to the nearest chat room? Or do you investigate? Have you read the relevant docs? How are your googling skills? Do you know how to use a debugger and a profiler? **Mastering the tools and techniques of troubleshooting will save you (and the chat room!) pain and time!**

