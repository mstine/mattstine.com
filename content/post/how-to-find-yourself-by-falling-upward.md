---
title: "How to Find Yourself by Falling Upward"
date: 2021-08-28T00:00:00-05:00
categories: ["atomic-essay","deconstruction"]
---

***When you hear the word fall, you probably think of a downward motion.***
  
**Fall** is almost always followed by **down**. Gravity, which causes objects to **free fall,** is intuitively understood as a **downward force.** Even our **Fall season** takes its name from the annual phenomenon of dead **leaves falling down** from trees.  

### Paradoxically, we can describe the journey of Deconstruction as falling upward.
  
*Falling Upward*, by Richard Rohr, provided a framework for understanding my deconstruction journey.  
  
Rohr describes two halves of life -- halves not of time, but of spiritual focus. The first focuses on ego work, while the second focuses on soul work. The two halves are separated by a traumatic but transformational fall.  

### This framework allowed me to focus on the work of Deconstruction rather than the phenomenon itself.
  
Let's look at these three phases:  

## #1: Building Our Box
  
During the first half of life, we are building our box.  
  
**Our box represents our ego's needs: identity, security, boundaries, order, safety, etc.** We form our conceptual understanding of how the world works, and how to behave in that world. We choose our friends, our careers, and perhaps our life partner.  
  
***A place for everything, and everything in its place.***  

## #2: Discovering the Limits of Our Box
  
Eventually, we encounter something our box cannot contain.  
  
**An anomaly. A contradiction. Some trauma requiring us to "think outside the box."** We fight and we struggle to make it fit, but we find the task impossible. The world simply doesn't work the way we thought it did.
  
***And so we fall upward.***  

## #3: Filling Our Box
  
Upward to the second half of life, when we stop building and start being.  
  
**We stop trying to prevail. Rather than fear mystery and uncertainty, we embrace them. We stop wearing masks. Rather than hide our true self, we reveal it.** Rohr calls this a *second simplicity*, a return to child-like innocence coupled with mature wisdom.
  
***We rest knowing that God is found somewhere within the universe, giving it purpose and direction. And we learn to trust reality, rather than trying vainly to protect ourselves from it.***
