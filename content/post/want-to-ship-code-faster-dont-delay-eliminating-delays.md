---
title: "Want to Ship Code Faster? Don't Delay Eliminating Delays"
date: 2021-09-28T00:00:00-05:00
categories: ["atomic-essay","seven-wastes-series","waste","agile","lean"]
---

***Delays are the fifth of the Seven Wastes of Software Development.***
  
The Seven Wastes were identified by Mary and Tom Poppendieck, the foreparents of Lean Software Development. It grew out of their efforts to understand and apply the principles of Lean Manufacturing to software engineering. **Waste elimination is the essence of Lean's archetype, the Toyota Production System:**  
  
> *All we are doing is looking at the timeline from the moment a customer gives us an order to the point when we collect the cash. And we are reducing that timeline by removing the non-value-added wastes.*

## Eliminating waste is about increasing speed.
  
Delays obviously slow us down.  
  
**Delays are software development's analog to manufacturing's Waiting.** If those don't seem that different, it's because they aren't. In both cases, we are either delaying the start of a value-adding activity or making a value-adding activity take longer than it should.
  
***Delays should be the most visible waste we can eliminate!***

## What's wrong with Delays?
  
**Delays slow down the delivery of value.** Even if you only perform value-adding work, delays can negatively affect value (lowering your customer's competitive advantage, profit margin, etc.).  
  
**Delays introduce discontinuity into your process, and just as Type-O blood is the universal donor type, discontinuity is the universal waste creator.** Discontinuity always spawns instances of the other wastes:  

- Delays trigger **Task Switching**. If you have to wait for information, then you can either sit idle or switch to another task.
- By triggering task switching, delays create **Partially Done Work**.
- Delays fuel **Relearning** by lengthening feedback loops.
- Delays fuel **Extra Features** through those same lengthened feedback loops.
- Delays between coding and testing increase the impact of **Defects** by allowing them to lie undetected longer.

## So what are some examples of Delays?

- Starting development work long after user stories have been written.
- Waiting for people to become available to start work.
- Lengthy attempts to exhaustively document every aspect of a project.
- Review or approval processes gating development phases and requiring attention from scarcely available individuals.
- Increased work in progress. Think about what happens on the freeway at rush hour. The more cars you try to shove onto the road, the slower the flow of traffic becomes
- Gaps between the completion of development and the beginning of testing.
- Gaps between the completion of testing and the beginning of deployment.

