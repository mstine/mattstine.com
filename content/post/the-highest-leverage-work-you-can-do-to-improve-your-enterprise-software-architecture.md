---
title: "The Highest Leverage Work You Can Do to Improve Your Enterprise Software Architecture"
date: 2021-08-25T00:00:00-05:00
categories: ["atomic-essay","software-architecture","leverage","paved-paths","choice-architecture"]
---

**You've finally made it. You've been promoted to Enterprise Architect.** 

For many, this is the pinnacle of their career. For most, they arrive in their new role with an existential question:  

## What do I do?  
    
Nearly every organization that's been around longer than FAANG companies is engaged in some form of software architecture modernization initiative. Over the course of my 21-year career, I've been privileged to work with dozens of them.  
    
***I've found one activity that will move the architecture modernization needle more than any other.***

- It's not more reuse.  
- It's not better governance.  
- It's not migrating to the cloud.  
- It's not decomposing monoliths into microservices.  
    
All of these things can be helpful. Or harmful.  
    
But what will move the needle in the right direction every single time?  

## Make the right thing to do the easiest thing to do.  
    
It really is that simple. Not only that, it's based on our understanding of fundamental human behaviors.  
    
James Clear, author of *Atomic Habits*, writes:

>*Energy is precious and the human brain is wired to conserve it whenever possible. As a result, we often find ourselves sliding into the most convenient option, not necessarily the best option.*  
    
Shane Parrish, founder of Farnam Street Media, makes the idea even simpler:

>*We spend far more time doing what's easy than doing what's right.*  
    
*__Rest assured, when given options, software developers and architects in your organization will choose the easiest option every single time. It's how we're wired.__*  

## So what can you do?  

- Should applications only get data from authorized sources? **Make those sources easy to discover, understand, securely access, and use.**  
- Should applications only use approved frameworks and libraries? **Create automated templates for starting new projects with those frameworks and libraries.**  
- Should architecture documentation be kept current and accurate? **Provide tooling to introspect the code and generate as much documentation as possible during the build.**  
    
These are only a few examples. But once you start thinking this way, the list of high-value improvement activities you can start tomorrow writes itself.
