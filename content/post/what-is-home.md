---
title: "What Is Home"
date: 2021-09-15T00:00:00-05:00
categories: ["atomic-essay","poetry"]
---

Home is where you wake up, and you don't wonder where you are.
  
Home is where you look around and you think, "Yes. This is how it should be."  
  
Home is where your partner in life lives, even when they are away.  
  
Home is where you kid hangs out, just because they want to.  
  
Home is where your 11 year old "puppy" is constantly by your side.  
  
Home is a feeling, but it is also very much a place.  
  
It is a place with a unique feel. A culture of its own.  
  
It is the true melting pot. Where all of the many cultures  
  
and traditions  
  
and spiritualities  
  
and philosophies  
  
and  
  
luck of  
  
the  
  
draw  
  
... come together to create the uniqueness that is you.  
  
And then you realize that you *are* home.  
  
And home is wherever you truly are.  

## I did not set out tonight to write a poem. But that's what came out.

  
I was just sitting here thinking about how I needed to start my writing streak again. And how I didn't really have the energy to write my typical atomic essay. And then my mind drifted to a conversation I'd had earlier with my fiancée about home.  
  
And rather than just think, I decided to just write. Whatever came out.  
  
And in the words of Roy Kent, **"F*CK!"**  
  
***I wrote 217 words, and created this, just letting my mind spill onto the keyboard.***
