---
title: "How I Turned a $200 Carrot Into a Writing Habit"
date: 2021-08-14T00:00:00-05:00
categories: ["atomic-essay","writing","resistance","ship30for30","dzone"]
---

It's Day One of shipping an Atomic Essay every day for 30 consecutive days. Ship 30 for 30 gave us a recommended day one prompt, and I am here for it. **So why are we going on this journey in the first place?**  
  
> I really needed a firm kick in the ass to get back into writing.  
  
I've published dozens of articles. I've even published a book! But it's been a very long time since I described myself as a writer.  
  
**These days I find within myself two opposing forces:**  

- An unquenchable thirst to write  
- A powerful resistance to sitting down to write  
  
**I know it wasn't always this hard. Was there ever a time when writing came naturally?**  
  
Yes.  
  
In 2010, I was a Zone Leader at DZone.com, one of the biggest aggregators of software development content. I was early career, just trying to establish my brand. And I jumped at the opportunity to be paid $200/month to write two articles per week. I did it for six months.  
  
And I never missed writing an article.  
  
**That $200 carrot was enough motivation to establish a regular writing habit.**  
  
What's my carrot this time?  
  
> The desire to reclaim that feeling.  
  
Getting my ideas out of my head and into the world. Finding out what's really worth writing. Because it has an audience, and because that audience gains value from my ideas. I don't need that $200 carrot anymore, because I know the cash wasn't really driving me. It was only the catalyst that ignited the fire.
