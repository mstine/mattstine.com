---
title: "The Perfect 10 That Wasnt So Perfect"
date: 2021-08-23T00:00:00-05:00
categories: ["atomic-essay"]
---

I did not want to write today. I still don't.
  
Today is supposed to be a perfect day. A "Perfect 10." The tenth consecutive day that I've published as part of Ship 30 for 30.  

### It's been a stressful few weeks:

- I found out I had **90 days to move out** of a rental house I had no intention of leaving any time soon.
- It's a seller's market, and **buying a house** right now feels like the closest thing to a **"living hell"** I've experienced in a while. The rental market is worse!
- I'm currently living in Mississippi. Do you know what we're famous for? Besides lynchings? We've simultaneously become known as the **highest per capita COVID-19 state** (not just in the U.S., the world!), as well as the **lowest per capita vaccination state**.
- Two of my family members **have already tested positive** for the good ole Delta Variant since "masks optional" school began. One ended up in the hospital, thankfully for less than 24 hours.
- And by the way, **all of the regular dumpster fires you can find on a doomscroll near you are still burning!**

## That may not be a lot for you, and plenty of people have it *much worse*, but it's a lot for me.
  
***I. AM. EXHAUSTED.***  
  
But here I am. Writing another atomic essay. I'm going to ship it. Because a couple of weeks ago I signed up to publish 250-ish words per day for 30 consecutive days.  
  
It's a little like working out.  
  
You see, I pretty much hate working out. But I do it 4-5 times a week. Why? **Because it's what I do.** *It's a good habit, and it helps me stay healthy. Plenty of habits are simultaneously as good for you as they are unpleasant.*
  
So here I am. Getting ready to hit publish. **Because it's what I do.**  
  
***Because I am a writer.***
