---
title: "Want to Ship Code Faster? Find Defects Faster"
date: 2021-10-09T00:00:00-05:00
categories: ["atomic-essay","seven-wastes-series","waste","agile","lean"]
---

***Defects are the last of the Seven Wastes of Software Development.***
  
The Seven Wastes were identified by Mary and Tom Poppendieck, the foreparents of Lean Software Development. It grew out of their efforts to understand and apply the principles of Lean Manufacturing to software engineering. **Waste elimination is the essence of Lean's archetype, the Toyota Production System:**  
  
> *All we are doing is looking at the timeline from the moment a customer gives us an order to the point when we collect the cash. And we are reducing that timeline by removing the non-value-added wastes.*

## Eliminating waste is about increasing speed.
  
So how do Defects slow us down?  
  
**Defects are the lone waste shared directly between software development and manufacturing.** They are exactly what you think they are. According to the Poppendiecks, the waste created by a defect is found through a simple formula:  
  
`WASTE = IMPACT * TIME TO DISCOVERY`
  
Which of these can we control? Clearly, none of us start our workday thinking, "I'm only going to write low impact Defects today." The power to control the impact of our Defects would be the power to write perfect code.  
  
***To find Defects faster is to ship code faster.***

## So how do we find Defects faster?
  
The answer is not shocking: ***an effective set of testing and CI/CD practices.***

- **A relatively comprehensive automated regression test suite.** It covers 70-80% of our code, and it covers 100% of the high probability situations our code will encounter.
- **Judicious use of exploratory testing.** It must be conducted by testers that think like customers. They should try everything an engineer believes a customer will never do.
- **Antibody tests.** Find a bug? Write a test that fails when the bug is present. Make the test pass. The more you embrace this practice, the better your software's immune system will be.
- **Combinatorial testing tools.** They start where our human-authored tests stop. They can exercise our functions with numerous possible (but low probability) inputs and report on their behavior.
- **Architectural Fitness Functions.** Describe your desired architectural characteristics with automated tests. Run these tests as your software evolves to ensure that it retains these characteristics.
- **A robust Continuous Integration/Delivery system.** In today's world, your CI/CD environment is a critical production-grade environment. Builds must execute immediately, quickly, and reliably, or your testing investment will turn to waste.

