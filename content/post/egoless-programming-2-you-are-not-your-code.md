---
title: "Egoless Programming #2: You Are Not Your Code"
date: 2022-01-16T00:00:00-06:00
slug: egoless-programming-2-you-are-not-your-code
categories: ["atomic-essay","egoless-programming","humility-driven-development","software-engineering","principles"]
---

In 2001, Lamont Adams of TechRepublic chiseled what would become ten commandments of Gerald M. Weinberg's timeless wisdom: **Egoless Programming**. Today we're going to learn its second commandment:  
  
> *You are not your code.*  
  
This is without a doubt one of the hardest lessons for any early-career engineer to learn. As humans, we take pride in our creations. Unfortunately, software is soft because it's meant to be changed, rewritten, and very often discarded completely. Otherwise it would be...hardware. So, when we derive our self-worth from our code, our self-worth is constantly under attack.  

## Your code will be critiqued.  
  
You will often write suboptimal code. Your teammates will notice.  
  
It's happened to me while pairing, in formal code reviews, and on pull requests. It can definitely sting. But it's always an opportunity to learn and improve.  

## Your code will be refactored.  
  
There's almost always a better design. Your teammates will provide it.  
  
One morning after a `git pull` I found a new algorithm in place of mine. It was dense, elegant, superior to my solution, and written by an early-career engineer. It was an opportunity to be proud of my teammate instead of disappointed in myself.  

## Your code will be optimized.  
  
Your code may work but not be efficient enough. Your teammates will tune it.  
  
I wrote what I thought was the most obvious, easy-to-understand code to process a large dataset. Unfortunately, it was also disastrously memory inefficient and routinely caused container crashes. My teammate rewrote it. It was an opportunity to learn how to optimize for a different set of tradeoffs.  

## Your code will be deleted.  
  
Your code may be valuable today but costly tomorrow. Your teammates will remove it.  
  
I spent several days writing the storage logic for an API. Eventually the data store was replaced with a more resilient solution, and my code was sent to `/dev/null`. It was an opportunity to focus on the value of the customer solution rather than the value of my code.  

## All of these events are part of the daily life of a software engineer.  
    
And they say nothing about your inherent worth as an engineer, a colleague, or a human.
