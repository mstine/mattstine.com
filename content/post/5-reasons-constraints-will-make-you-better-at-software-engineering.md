---
title: "5 Reasons Constraints Will Make You Better at Software Engineering"
date: 2021-08-31T00:00:00-05:00
categories: ["atomic-essay","software-architecture","constraints","software-engineering"]
---

*__The best thing you can do for your software engineering ecosystem is to add constraints.__*  
    
What do I mean by constraints?

Anything that limits the degrees of freedom you have when building a software system. You might call them:  

- limitations  
- restrictions  
- standards  
- contracts  
- controls  

# Regardless of what you call them, constraints will make your team better.  
    
Here are five reasons why:  

## #1: Constraints limit the number of decisions you have to make.  
    
Mary Poppendieck wrote, "Developers make critical decisions about every 15 minutes." That said, just because a decision is critical, it doesn't have to be made for every system. 

*__Picking a standard framework and database for all systems allows you to focus decision-making power on the logic that delivers business value.__*  

## #2: Constraints make it easier for engineers to float between systems.  
    
In an ideal world, teams would stick together for the entire life of a system. Unfortunately, business demand for any given product will always ebb and flow. It's both inefficient and inhumane to have one team sitting idle while another is barely keeping up. 

*__Constraining languages, frameworks, and architectural styles flattens the learning curve for developers floating into a team, accelerating their ability to share the load.__*  

## #3: Constraints make work more algorithmic.  
    
Variability in system design and architecture breeds wasted human effort. Rather than focusing on adding business value, we're designing yet another deployment process or test plan. By constraining the way you build, test, and deploy systems, you create an algorithm for performing those tasks. 

*__Algorithms breed automation. Automation breeds efficiency.__*  

## #4: Constraints make system integration smoother.  
    
In today's connected world, no software system is an island. Systems must integrate with other systems. Why are LEGO bricks so powerful? Because they have a constrained integration mechanism. Anyone can learn to build with them rapidly. 

*__By creating standard integration mechanisms for your software, you can eliminate a lot of the burden.__*  

## #5: Constraints make it easier to reason about your software's behavior.  
    
Eventually, software needs to land in production. If every system is a snowflake, every runbook will be also. You shouldn't have to reason from first principles about every single incident your software creates. 

*__Placing constraints on what it can and cannot do limits the cognitive cost of troubleshooting incidents and accelerates the restoration of service to customers.__*
