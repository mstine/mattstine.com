---
title: "Want to Ship Code Faster? Stop Learning Things Twice"
date: 2021-09-22T00:00:00-05:00
categories: ["atomic-essay","seven-wastes-series","waste","agile","lean"]
---

***Relearning is the third of the Seven Wastes of Software Development.***
  
The Seven Wastes were identified by Mary and Tom Poppendieck, the foreparents of Lean Software Development. It grew out of their efforts to understand and apply the principles of Lean Manufacturing to software engineering. **Waste elimination is the essence of Lean's archetype, the Toyota Production System:**  
  
> *All we are doing is looking at the timeline from the moment a customer gives us an order to the point when we collect the cash. And we are reducing that timeline by removing the non-value-added wastes.*

## Eliminating waste is about increasing speed.
  
So how does Relearning slow us down?  
  
**Relearning is software development's analog to manufacturing's Extra Processing.** Learning delivers value. It helps us build better products, and it helps us deliver products faster. If we have to repeat a value-adding activity, we can only assume we lost the original value. The more times we repeat it, the more waste we create.  

## A few of the other wastes are "gateway drugs" to Relearning:
  
**Handoffs naturally cause knowledge to degrade as it is passed from person to person.** Eventually, that knowledge must be "relearned" to provide value.  
  
**Delays fuel relearning by lengthening feedback loops.** Requirements gathered far in advance become obsolete and must be regathered. Defects that lie undetected force developers to stop and rebuild context.  
  
**Task switching leads to continual relearning.** Every time you switch tasks, it takes you an average of fifteen minutes to "reload" a task into your brain before you can be productive.  

## So what are some examples of Relearning?

- Poor knowledge capture often leads to the rediscovery of the same knowledge. Developers routinely "fix and forget," only to later repeat the cycle.
- Often we have access to subject matter experts. We ignore them at our peril. Everything we learn on our own is "relearned knowledge," and every minute we spend "doing our own research" is a wasted minute.
- Poorly written code obscures its intent and behavior and forces us to relearn every time we need to maintain it. Undocumented yet well-written code that manages significant essential complexity leads to the same result.
- Haphazard project management also generates relearning. If we constantly reassign developers and they leave their current work unfinished, each new developer assigned must relearn the knowledge already in the original developer's possession.
  
***We can eliminate relearning by more effectively capturing and preserving knowledge. Shared knowledge bases, refactoring, well-written automated tests, team-based problem solving, and cultures of continuous improvement can all build knowledge into the life of your team.***

