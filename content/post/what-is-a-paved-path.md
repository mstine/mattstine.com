---
title: "What Is a Paved Path?"
date: 2022-01-11T00:00:00-06:00
categories: ["atomic-essay","software-architecture","leverage","paved-paths","choice-architecture"]
---

*__I'm going to explain to you the concept of a paved path to software delivery.__*  
    
A paved path will make your engineering job as easy as possible. It will complete every task that wastes your time and effort. It will keep you focused on delivering business value through technology solutions.  
    
Sound good? Excellent. Let's dive in.  

## It enables getting started with a clean architecture:  
    
It's a lot easier to get started well than to get started poorly and fix it later.  
    
We already know that absent intentional team effort, every codebase rots. Why start with anything less than the cleanest architecture your organization can produce? 

***Paved Paths*** provide a skeleton for your unique domain model.  

## It paves the path to production:  
    
Non-value-adding variation reduces testability and deployability.  
    
Continuous Delivery taught us we should always keep the codebase deployable. To do that, it must also be testable. 

***Paved Paths*** ensure your code plays well with the standard CI/CD infrastructure, deployment environment, and every additional tool in between.  

## It solves for the 80%:   
    
If you build the same types of applications 80% of the time, why start from scratch?  
    
Do you really remember what versions of what dependencies you need? Do you really remember how to integrate with the company authorization platform? 

***Paved Paths*** provide curated and tested implementations of solutions to common problems.  

## It does not remove the team's autonomy:   
    
The team that builds the system is best equipped to make decisions about its design and architecture.  
    
***Paved Paths*** get you started in a known good state. But they don't prevent you from deviating from the path when the team deems it necessary. And the team should be making those decisions, not an architecture committee.  

## It is not handed down from the ivory tower:   
    
Patterns are discovered, not invented. They are Aristotelian, not Platonic.  
    
***Paved Paths*** are born from trails already blazed to production. They represent the best knowledge the organization has about delivering valuable lines of code to customers. They stand on successful shoulders that have already shipped.  

## I hope you get at least one opportunity to code your way down a well-paved path.  
    
And best of luck to you as you focus on delivering business value.
