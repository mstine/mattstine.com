---
title: "Something Weird Most People Don't Know About Software Engineering"
date: 2022-08-25T00:00:00-06:00
categories: ["atomic-essay","software-engineering"]
---

There are many things most people don't know about working in Software Engineering.  
    
For example, did you know...  

- We reinvent the foundational dogma of our entire industry every 7-10 years. I've seen three cycles!  
- The equivalent of a graduate-level education with multiple software engineering specializations is freely accessible on the Internet, and most engineers don't look at it.  
- We have overwhelming volumes of data that tell us specific software engineering models work, and we do the opposite most of the time.  
    
If you've been a software engineer, you know what I'm talking about.  
    
But to those who don't jockey a mechanical keyboard all day to make pay, you might be saying, BRUV.  
    
For example, here's the weirdest thing most people don't know about Software Engineers:  

## Writing Code is the thing we do the least.  
    
I know, I know. You're gonna tell me that I'm like this super senior engineering director, and I've got direct reports, and of course, I don't write code all day.  
    
I didn't say anything about myself. Or me now.  
    
I Google a lot. I dig through internal and external documentation. I read other people's code. I read through my notes. I go to stand up. I go to town hall meetings for various execs. There's a sprint review here and an architecture review there. Maybe fight with Jira for a while. Do you know Jira? I hope not, for your sake.  
    
And we haven't even really gotten started on Zoom. I mean, a couple of those items might be Zooms. But there's also this energy vacuum that comes with Zoom. The energy you can expend writing code is inversely proportional to your time on Zoom.  
    
All that to say, I do a lot of things that I still file under Software Engineering. But it sure isn't banging out Kotlin or Go.  
    
Crazy, right?
