---
title: "As a Software Engineer I Want to Use the No Free Lunch Principle So That I Can Save Myself Some Pain"
date: 2021-08-19T00:00:00-05:00
categories: ["atomic-essay","software-engineering","principles","no-free-lunch","tradeoffs"]
---

*In 2008, I believed the only way to save a critical software system was to rewrite it from scratch.*  
    
I had just been promoted to engineering manager of my team, and that system was our biggest project. And it was in trouble. I knew we had made a lot of mistakes along the way. Now that I was "in charge," I was going to fix everything.  
    
That rewrite almost failed, and I nearly got myself fired.  

## No technology, technique, pattern, or practice will buy you a free lunch.
    
If you don't internalize this principle, you're going to experience a lot of pain.  
    
Software projects always run into problems. If you're always looking for the proverbial "silver bullet" to solve them, you'll always be disappointed. That's why Fred Brooks wrote "No Silver Bullet" in 1987. Because it doesn't exist. The quicker you learn this, the quicker you can get on to actually solving your problems rather than creating new ones!  

### #1. Understand that every solution comes with tradeoffs.
    
You can't solve a problem without giving something up in exchange.  
    
This is what "no free lunch" means. You'll probably get value from adopting modern tech, best practices, and design patterns. But you're going to pay for it.  

### #2. Evaluate the costs of every potential solution to your problem.
    
Everyone will tell you what's awesome about their solution. But they won't volunteer its tradeoffs. That's why it's your job to:  

- Ask around your company. Find out if anyone has used the solution before.  
- Read what industry analysts have to say.  
- When all else fails, ask the Internet (Quora, Twitter, etc.)  

### #3. Make sure the value provided by your choice exceeds its costs.
    
This part can be tricky, as cost and value can often be hard to quantify. Creating a scorecard with a points system can help:  

- Invent a points system. Nothing more complicated than 1 to 3 or 1 to 5.  
- List the characteristics or categories you care about.  
- Value gets a positive score, costs get a negative score.  
- Tally up the points. If you're greater than zero, you can at least consider the solution viable.  
Looking back, if I'd done this in 2008, that rewrite would have never happened. Learn from my mistake and save yourself some pain!
