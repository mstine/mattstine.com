---
title: "Every Scout Knows the Secret to Eliminating Software Engineering Toil"
date: 2021-08-27T00:00:00-05:00
categories: ["atomic-essay","software-engineering","clean-code"]
---

***If you've been a software engineer for very long, you've probably worked on a codebase that's seen better days.***
  
It's not clear where the next feature should be added. Some components do too much, and some components do too little. Every change you make seems to break something else. Writing software has transformed from delight to drudgery. **Your task list has become a toil list.**  

## But it doesn't have to be this way.
  
As a boy, I spent many happy years in a scout troop. Camping was the highlight. And by the time I returned from my first trip, I'd learned **the cardinal rule of camping**:  
  
> *Always leave the campground cleaner than you found it.*  
  
Leaving the campground cleaner was the only way the next campers could enjoy it. Who wants to arrive at their destination only to spend their first hour picking up trash? We didn't, and we knew the next group wouldn't either. **So before we left, our troop lined up and marched from one end of the campground to the other, picking up anything that didn't belong.**  
  
***So what does this have to do with software engineering?***  

## Your codebase is your campground.
  
It didn't start out trashy, and no (good) engineer deliberately creates chaos. But every codebase has a trajectory. Leaving one smelly bit of code today doesn't make much difference. Repeating that every day? You know the rest of the story.  
## How can you apply scouting's rule to coding? Start by adding these practices to your workflow:

- **Refactor first.** If the current design makes adding a feature hard, improve the design then add the feature.
- **Add a test daily.** Are parts of your system uncovered? Adding a single test to uncovered code daily will add approximately 200 tests per year.
- **If you see something, fix something.** Is a variable or function poorly named? Rename it. Is a function too complex? Break it up.
- **Enable reformat on save.** Nearly every editor does this now, and it ensures your code always conforms to the agreed-upon style.
  
***These practices place your codebase on a different trajectory. Fixing one variable name today doesn't make much difference. Repeating that every day will rewrite the story.***
