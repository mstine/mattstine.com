---
title: "The One Software Tool I Can't Live Without for Doing Polymathic Knowledge Work"
date: 2022-08-29T00:00:00-05:00
categories: ["atomic-essay", "tools", "polymath", "pkm", "logseq"]
---

Today, there is an app, website, or software platform for almost anything (including an app that does absolutely nothing).  
  
But when it comes to being a polymath who's learning, thinking, and creating all day, there is one app I can't live without:  

## Logseq  
  
And here's why:  

### I never have to leave my Daily Journal  
  
Logseq opens to a new journal page daily, and it always structures this page as an outline. Every item ("block") in the outline is time-stamped, directly addressable, and parent-child relationship aware.  
  
Once you understand the consequences of these facts, you'll realize you don't ever need another kind of page in your notes.  

### If I link it, I can't lose it.  
  
Logseq makes it trivial to link any block to a term/page/tag/etc.  
  
Just put the  `[[term]]`  in double-square brackets. Hit <ENTER>.  
  
Click on the hyperlinked term, and check your "Linked References." Is your mind blown?  

### I manage my tasks in context.  
  
Any block can instantly become a task.  
  
I'm a Linux user, so I'll hit <CTRL>+<ENTER>+<ENTER>. That should work for you as well Windows users. Mac folk? You know what to substitute.  
  
An empty checkbox labeled LATER appears to the left of my content. And all of the notes that surrounded it are still there.  

### Patterns effortlessly become templates.  
  
I love routine workflows, and I try to systematize almost everything I do while I'm working.  
  
As soon as I see that I've built similar structures for a few projects, or I decide I want to track a type of data using a specific schema, I'll right-click on the bullet adjacent to that structure and choose "Make template."  

### Query mastery gives you superpowers.  
  
`{{query (task LATER)}}`   
  
That little code snippet will find every task labeled LATER in your entire graph. It presents your tasks in context or a table containing rows of task blocks. A superpower on its own, this little code snippet represents only the tip of the query iceberg.  
    
I can't recommend using this app enough if you work with dynamic webs of diverse ideas.
