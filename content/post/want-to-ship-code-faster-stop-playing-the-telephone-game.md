---
title: "Want to Ship Code Faster? Stop Playing the Telephone Game"
date: 2021-09-27T00:00:00-05:00
categories: ["atomic-essay","seven-wastes-series","waste","agile","lean"]
---

***Handoffs are the fourth of the Seven Wastes of Software Development.***
  
The Seven Wastes were identified by Mary and Tom Poppendieck, the foreparents of Lean Software Development. It grew out of their efforts to understand and apply the principles of Lean Manufacturing to software engineering. **Waste elimination is the essence of Lean's archetype, the Toyota Production System:**  
  
> *All we are doing is looking at the timeline from the moment a customer gives us an order to the point when we collect the cash. And we are reducing that timeline by removing the non-value-added wastes.*

## Eliminating waste is about increasing speed.
  
So how do Handoffs slow us down?  
  
**Handoffs are software development's analog to manufacturing's Transportation.** Transportation refers to the moving of goods between distant locations. Bad things often happen when we transport goods. They can be:  

- Lost
- Damaged
- Late
- Sent to the wrong location
  
**Handoffs do the same thing, except they do it with knowledge.** And the same bad things can happen when knowledge is handed off.  

## The key challenge with Handoffs is tacit knowledge.
  
Wikipedia defines tacit knowledge as ***knowledge that is difficult to transfer to another person by means of writing it down or verbalizing it***. Let's look at some examples:  
  
**Eleven years ago, my then 5-year old daughter asked me how to blow a bubble with gum.** How do you explain that? How much tacit knowledge is involved? Technically I could explain how to do it, but how likely is it she'll be able to follow my instructions?  
  
**Did you ever play the telephone game as a child?** How many "handoffs" did it take before "Johnny picked an apple from the tree" became "Johnny picked his nose by the sea?"  
  
***That's exactly what's happening with handoffs. We're losing the message bit by bit as it goes down the line.***

## How big is this problem?
  
The Poppendieck's suggest a conservative estimate where each handoff leaves behind 50% of the knowledge. That means:  

- 25% of knowledge left after 2 handoffs
- 12% of knowledge left after 3 handoffs
- 6% of knowledge left after 4 handoffs
- 3% of knowledge left after 5 handoffs

## So how do we tackle the problem of Handoffs?

- Try to reduce the number. Find ways to integrate teams that need to work together.
- Use cross-functional teams composed of analysts, architects, developers, and testers.
- Use high-bandwidth communication methods. A good pecking order: Face-to-face, Zoom calls, chat, email, documents (present challenges with COVID-19 acknowledged!).
- Document knowledge where necessary. Use lightweight markup with version control to encourage evolution and freshness.
- Quicken your feedback loops. Shorten your iterations. Close the gaps.

