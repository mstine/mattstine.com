---
title: "wip microservices reading list"
date: "2014-07-02"
Categories: ["microservices", "architecture"]
---

I've started curating a [Microservices Reading List](/microservices). It's still work in progress, but there's some good stuff there. Watch for more!
