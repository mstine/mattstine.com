---
title: "Want to Master Enterprise Software Engineering Use These Three Power Moves to Achieve Great Success"
date: 2022-08-26T00:00:00-05:00
categories: ["atomic-essay","software-engineering","satire"]
---

![Borat - Great Success!](images/borat.jpg)

I have been in the “enterprise software engineering” industry for 22 years.  
    
During that time, I have briefly but technically become a cloud startup millionaire. I published the first book with “cloud-native” in the title (and yes, the goddamn hyphen is correct, I said what I said). In fact, I have invested so many hours into mastering my craft that I literally have to turn down invitations to speak at conferences on the regular, even in this post-COVID era.  
    
But did you notice something?  
    
None of those have anything to do with enterprise software engineering.  
    
We tend to revere personalities, celebrities, and influencers - label us what you will - on social media, but geek fame is worthless inside the belly of corporate technology.  
    
So I’ve had to learn a few power moves to progress throughout most of my pseudo-technical career. Here are my most potent 3:  
    
## Power Move #1: There’s always more room on the slide.
    
Here’s how it works:  

- Move the company floodmarks (you know, on the PowerPoint template) as far into the margin as you can.  
- Fill at least six boxes with bulleted lists of 10pt. text.  
- Paste them into a grid on the slide. Repeat until painful.  
    
This simple template lets you say almost anything you want during a presentation. Your audience will spend the entire time trying to decipher the hieroglyphics on your screen share.  
    
## Power Move #2: Never underestimate the power of the one.
    
Here’s how it works:  

- Schedule a one-hour meeting.  
- Create an intro slide with an agenda.  
- Spend 30 minutes cramming multiple random topics into the first bullet point.  
    
This move allows you to “[take out the trash](https://www.youtube.com/watch?v=-Bn08Ju_eB8)” by jamming through several unsavory items quickly, followed by a quick palate cleanser. No one will remember the negative bits, and the slides say, “Update.”  
    
## Power Move #3: Project confidence even when you’re incompetent.
    
Here’s how it works:  

- Avoid delivering tangible solutions. That sounds suspiciously like work.  
- Instead, show up to as many meetings as possible with your complicated slides and confidently make unfounded assertions.  
- If you avoid work and provide plausible objections to every legitimate technology initiative, you’ll unlock a long and successful career as an enterprise mediocrity architect.  
    
Easy, right?  
