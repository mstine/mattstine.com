---
title: "The Ten Timeless Commandments of Egoless Programming"
date: 2022-01-13T00:00:00-06:00
categories: ["atomic-essay","egoless-programming","humility-driven-development","software-engineering","principles"]
---

In 1971, Gerald M. Weinberg wrote[^EN] these Ten Commandments upon the stone tablets of *The Psychology of Computer Programming.*  
    
Well, they weren't really written in stone. But they have stood the test of time. Wise is the software engineer who learns them and puts them into practice.  
## 1. Understand and accept that you will make mistakes.  
    
Assume that you will write bad code. Find it and fix it before it affects users.  
## 2. You are not your code.  
    
Software is meant to be changed, rewritten, and discarded. You are not.  
## 3. No matter how much "karate" you know, someone else will always know more.  
    
Be humble. Ask for help. Learn from anyone and everyone.  
## 4. Don't rewrite code without consultation.  
    
You have the code, but you don't have the whole picture. Have that conversation.  
## 5. Treat people who know less than you with respect, deference, and patience.  
    
They are humans. They deserve it. You need no further reason.  
## 6. The only constant in the world is change.  
    
Software is only "soft" so it can easily change. Embrace it.  
## 7. The only true authority stems from knowledge, not from position.  
    
The moment your title is the foundation of the respect you receive, you've lost the team.  
## 8. Fight for what you believe, but gracefully accept defeat.  
    
Debate vigorously. Rally behind your team's choice.  
## 9. Don't be "the guy in the room."  
    
Software delivery is a collaborative game played by humans. Don't hide behind your laptop. Play the game.  
## 10. Critique code instead of people.  
    
Software is meant to be changed, rewritten, and discarded. Your colleagues are not.

[^EN]: **EDITORIAL NOTE (2022-12-02):** At the time this was originally published, I was unaware that this list was actually produced in 2001 by Lamont Adams (of TechRepublic) and only inspired by Weinberg's "egoless programming" concept.
