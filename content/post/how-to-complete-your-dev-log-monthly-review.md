---
title: "How to Complete Your Dev Log Monthly Review"
date: 2022-02-04T00:00:00-06:00
categories: ["atomic-essay", "pkm", "dev-log", "software-engineering"]
---

Today we'll walk through the next summarization level we need in our dev logs: monthly reviews.
  
**We're slowly climbing a mountain:**  

- Daily logs tell us the quantum details of what we did on a specific day.
- Weekly reviews roll those details up into atomic accomplishments for the week.
- Monthly reviews compose those accomplishments into molecular deliveries.
  
Pardon the poor quantum physics analogy, but I think it's apropos. We are building what Tiago Forte calls "intermediate packets."  

## And, we can keep composing these packets into something larger:

- A copypasta exercise of writing your annual self-performance review.
- A presentation to a group of students about your career journey.
- A document prepared as part of a formal promotion or distinguished/principal engineer process.

## Bring two additional things to your monthly review:

- Questions you want to answer this month (e.g. what business value did I deliver?). Include questions you'll answer in your annual self-review.
- Progressive summarization, which I'll while **describing the monthly review process:**

### Step #1: Create a Monthly Review Note
  
Copy the month's weekly review notes into this note.  

### Step #2: Bold What Resonates
  
Read and apply a bold font-weight to anything that resonates as significant.  

### Step #3: Highlight Answers
  
Read the bolded notes and highlight anything that helps answer your prepared questions.  

### Step #5: The Executive Summary
  
Write a paragraph providing an elevator pitch for the month. Include answers to your questions.  
  
**That's it! We just finished January, so it's not too late to write your first monthly review. Go!**
