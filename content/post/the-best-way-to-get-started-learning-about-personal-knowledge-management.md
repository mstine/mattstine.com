---
title: "The Best Way to Get Started Learning About Personal Knowledge Management"
date: 2022-08-17T00:00:00-05:00
categories: ["atomic-essay","pkm","learning","resources"]
---

I love learning about Personal Knowledge Management (PKM)!  
    
I first got interested in this topic at least 15 years ago (it wasn't known as PKM then). It was overwhelming trying to figure out where to start at the time, and it's only gotten worse.  
    
Last year, I discovered Tiago Forte's online cohort-based course, *[Building a Second Brain](https://www.buildingasecondbrain.com/)*. Rarely will I reach for the word "life-changing," but the value of having so much curated knowledge experienced within a community has earned that label over the past year.  
    
The price was steep! I would not have afforded it 15 years ago. At this stage of my career, it's still not cheap, but it's an investment I wanted to make.  

## That said, if you'd like to learn about PKM without dropping $$$, here's the best way to get started:  

- **Read This Book:** *[Building a Second Brain](https://bookshop.org/p/books/building-a-second-brain-a-proven-method-to-organize-your-digital-life-and-unlock-your-creative-potential-tiago-forte/18265370?ean=9781982167387)* by Tiago Forte is now available in book form! I'm halfway through my copy, and I've found its value far exceeds its price tag.  
- **Watch This YouTube Video:** *[The Second Brain - A Life-Changing Productivity System](https://www.youtube.com/watch?v=OP3dA2GcAh8)* by one of my favorite creators of all time, Ali Abdaal. This video is a must-watch beginner PKM resource for anyone with only 15 minutes to spare.  
- **Listen To This Podcast:** If you want to start going down the rabbit hole, listen to the "[Building a Second Brain](https://www.artofmanliness.com/character/advice/podcast-816-building-a-second-brain/)" episode of Brett McKay's *The Art of Manliness* podcast. While the episode doesn't reveal a substantial amount of new information, there's something about this dialog that brings the concepts to life a little bit more.
