---
title: "TRIGGER WARNING: This post contains content that no longer represents me."
headless: true
class: is-danger
---
I have a really convoluted and confusing story. My friends that know it (including my wife) still don't believe it sometimes, but it's true.

I was once an evangelical Christian, a Southern Baptist, an adult Sunday School teacher, and a door-to-door evangelist. I not only voted Republican and/or Libertarian
in multiple elections, I volunteered during Bob Dole's 1996 election campaign. 

Reading what I just typed, it's honestly hard to believe it myself.

One day I plan to tell this story in written form, but that day is not today. Rather than erase that past completely, I've preserved it in a limited form
as a part of this site's permanent historical archive. It's important to demonstrate that we can all grow.
