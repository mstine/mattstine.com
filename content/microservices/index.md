---
date: "2014-07-02"
title: "Microservices Reading List"
---

Given the huge amount of buzz around Microservices right now, as well as the huge amount of content being generated, I thought I'd begin curating a "Microservices Reading List."
With that said, the main reason for the creation of this page is the continual question: "Great talk/article/etc. Where can I learn more?"


Below you'll find articles, blogs, videos, slide decks, etc.
I'm going to try to categorize them a bit as well.
This is by no means a complete catalog. Continue to check back for updates!

## Start Here

- [Microservices](http://martinfowler.com/articles/microservices.html): overview by James Lewis and Martin Fowler
- [Microservices](http://www.infoq.com/articles/microservices-intro): Decomposing Applications for Deployability and Scalability]: overview by Chris Richardson

## It's Not All Roses...

Here you'll find posts acknowledging the challenges associated with Microservices:

- [Microservices - Not A Free Lunch!](http://highscalability.com/blog/2014/4/8/microservices-not-a-free-lunch.html)
- [Microservices and the Failure of Encapsulation](https://michaelfeathers.silvrback.com/microservices-and-the-failure-of-encapsulaton) - interesting opinion piece

## Field Reports

Enough with the theory.
Who's doing this?

- [Building Products at SoundCloud —Part I: Dealing with the Monolith](http://developers.soundcloud.com/blog/building-products-at-soundcloud-part-1-dealing-with-the-monolith)
- [Building Products at SoundCloud—Part II: Breaking the Monolith](http://developers.soundcloud.com/blog/building-products-at-soundcloud-part-2-breaking-the-monolith)
- [Building Products at SoundCloud—Part III: Microservices in Scala and Finagle](http://developers.soundcloud.com/blog/building-products-at-soundcloud-part-3-microservices-in-scala-and-finagle)
- [How we build microservices at Karma](https://blog.yourkarma.com/building-microservices-at-karma): "Forget the hype, we're using microservices in production."
- [Microservices – the blind leading the blind](http://lonelycode.com/2014/04/08/microservices-the-blind-leading-the-blind/) - Field Report from [Loadzen](http://www.loadzen.com/)

## It's Not About Size

Don't get caught up in the *micro* of Microservices:

- [Micro services: It’s not (only) the size that matters; it’s (also) how you use them – part 1](http://www.tigerteam.dk/2014/micro-services-its-not-only-the-size-that-matters-its-also-how-you-use-them-part-1/)
- [Micro services: It’s not (only) the size that matters; it’s (also) how you use them – part 2](http://www.tigerteam.dk/2014/micro-services-its-not-only-the-size-that-matters-its-also-how-you-use-them-part-2/)
- [Microservices: It’s not (only) the size that matters; it’s (also) how you use them – part 3](http://www.tigerteam.dk/2014/microservices-its-not-only-the-size-that-matters-its-also-how-you-use-them-part-3/)
- [Microservices: It’s not (only) the size that matters; it’s (also) how you use them – part 4](http://www.tigerteam.dk/2014/microservices-its-not-only-the-size-that-matters-its-also-how-you-use-them-part-4/)

## Organization/Process/Governance

Microservices is far from being only a technical architecture pattern. There are huge issues associated with culture, team organization, process, data ownership, governance, etc.
Here are some relevant pieces:

- [Carving it up: Microservices Monoliths & Conway’s Law](http://genehughson.wordpress.com/2014/05/23/carving-it-up-microservices-monoliths-conways-law/)
- [More on Microservices: Boundaries Governance Reuse & Complexity](http://genehughson.wordpress.com/2014/06/04/more-on-microservices-boundaries-governance-reuse-complexity/)
- [Coordinating Microservices – Playing Well with Others](http://genehughson.wordpress.com/2014/06/16/coordinating-microservices-playing-well-with-others/)
- [Microservices and Data Architecture – Who Owns What Data?](http://genehughson.wordpress.com/2014/06/20/microservices-and-data-architecture-who-owns-what-data/)
- [API-Centric Architecture: Services Governance Does Not Scale](https://blog.apigee.com/detail/api_centric_architecture_services_governance_does_not_scale)

## Antifragile

My new friend Russ Miles is deep into Microservices. He's primarily focused on the meme of *antifragility* in software. Here's some of his stuff:

- [Antifragile Software: Building Adaptable Software with Microservices](https://leanpub.com/antifragilesoftware) - eBook in progress
- [Russ Miles on Antifragility and Microservices](http://www.infoq.com/articles/russ-miles-antifragility-microservices) - InfoQ Interview

## Useful Architectural Patterns

A lot of patterns are cropping up in the microservices space, many of which are described very well on Martin Fowler's site:

- [Bounded Context](http://martinfowler.com/bliki/BoundedContext.html)
- [Event Collaboration](http://martinfowler.com/eaaDev/EventCollaboration.html)
- [Circuit Breaker](http://martinfowler.com/bliki/CircuitBreaker.html)
- [Polyglot Persistence](http://martinfowler.com/bliki/PolyglotPersistence.html)
- [Tolerant Reader](http://martinfowler.com/bliki/TolerantReader.html)
- [Consumer-Driven Contracts](http://martinfowler.com/articles/consumerDrivenContracts.html)
- [Published Interface](http://martinfowler.com/bliki/PublishedInterface.html)
- [Event Sourcing](http://martinfowler.com/eaaDev/EventSourcing.html)

## Netflix!

Netflix is well known for employing microservices and talking about it. Here are several technical blogs that touch the subject in various ways:

- [Fault Tolerance in a High Volume Distributed System](http://techblog.netflix.com/2012/02/fault-tolerance-in-high-volume.html)
- [Embracing the Differences : Inside the Netflix API Redesign](http://techblog.netflix.com/2012/07/embracing-differences-inside-netflix.html)
- [Introducing Hystrix for Resilience Engineering](http://techblog.netflix.com/2012/11/hystrix.html)
- [Optimizing the Netflix API](http://techblog.netflix.com/2013/01/optimizing-netflix-api.html)
- [Deploying the Netflix API](http://techblog.netflix.com/2013/08/deploying-netflix-api.html)
- [Preparing the Netflix API for Deployment](http://techblog.netflix.com/2013/11/preparing-netflix-api-for-deployment.html)
- [The Netflix Dynamic Scripting Platform](http://techblog.netflix.com/2014/03/the-netflix-dynamic-scripting-platform.html)

## General Good Reads

Things that didn't really fit into a category of their own, but still tasty:

- [API-Centric Architecture: All Development is API Development](https://blog.apigee.com/detail/api_centric_architecture_all_development_is_api_development)
- [API-Centric Architecture: SOA Gives Way to Micro Services](https://blog.apigee.com/detail/api_centric_architecture_soa_gives_way_to_micro_services)
- [SoundCloud is Reading My Mind](http://dejanglozic.com/2014/06/16/soundcloud-is-reading-my-mind/)
- [Micro-service APIs With Some Swag (part 1)](http://dejanglozic.com/2014/06/25/micro-service-apis-with-some-swag-part-1/)
- [Micro-service APIs With Some Swag (part 2)](http://dejanglozic.com/2014/07/01/micro-service-apis-with-some-swag-part-2/)

## Conference Presentations

- [Micro Services: Java the Unix Way](http://www.infoq.com/presentations/Micro-Services) - James Lewis
- [Cloud Foundry and Microservices: A Mutualistic Symbiotic Relationship](http://www.slideshare.net/mstine/microservices-cf-summit) - Matt Stine (slides only, video coming soon)
- [Migrating to Microservices](http://qconlondon.com/dl/qcon-london-2014/slides/AdrianCockcroft_MigratingToMicroservices.pdf) - Adrian Cockcroft (slides only)
- [Microservices: Adaptive Systems for Innovative Organizations](http://thoughtworks.wistia.com/medias/gvb1mgw4r3) - James Lewis
- [Microservices Architecture](http://yow.eventer.com/yow-2012-1012/micro-services-architecture-by-fred-george-1286) - Fred George
- [Now Playing on Netflix: Adeventurs in a Cloudy Future](http://www.slideshare.net/adrianco/flowcon-added-to-for-cmg-keynote-talk-on-how-speed-wins-and-how-netflix-is-doing-continuous-delivery) - Adrian Cockcroft (slides only)


## Other Microservices Landing Sites

- [Microservice Architecture](http://microservices.io/) site by Chris Richardson
