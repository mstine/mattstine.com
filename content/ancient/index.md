---
title: Welcome to the dim corner of the library, where fools rush in and angels fear to tread!
headless: true
class: is-warning
---
This blog post is ancient. If it is technical, the information is likely inaccurate, or at least out of date. If it is non-technical, it's entirely possible that the relevant facts and my own opinions have changed significantly since it was written. 

It is only preserved as part of this site's permanent historical archive.
