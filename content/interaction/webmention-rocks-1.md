---
title: "Let's see if my remy/wm cron job works..."
date: "2022-12-06T00:00:00-0600"
response_type: "reply"
link: "https://webmention.rocks/test/1"
link_name: "Discovery Test #1"
---

If I did everything correctly, within five minutes of posting this interaction, it should show up as a mention on the Webmention Rocks! post.

